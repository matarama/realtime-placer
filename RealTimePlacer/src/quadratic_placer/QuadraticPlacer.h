/*
 * QuadraticPlacer.h
 *
 *  Created on: Jun 8, 2013
 *      Author: matara
 */

#ifndef QUADRATICPLACER_H_
#define QUADRATICPLACER_H_

#include "../vlsi_structure/Graph.h"
#include "../linear_solver/LinearEquationGenerator.h"
#include "../linear_solver/SparseConjugateGradient.h"

class QuadraticPlacer
{
public:
	QuadraticPlacer(Graph* graph);
	virtual ~QuadraticPlacer(void);

	void place(void);

	Graph* graph;
	LinearEquationGenerator* leg;
	SparseConjugateGradient* scgX;
	SparseConjugateGradient* scgY;
};

#endif /* QUADRATICPLACER_H_ */
