/*
 * QuadraticPlacer.cpp
 *
 *  Created on: Jun 8, 2013
 *      Author: matara
 */

#include "QuadraticPlacer.h"
#include "../boid/Position.h"

QuadraticPlacer::QuadraticPlacer(Graph* graph)
: graph(graph),
  leg(0),
  scgX(0),
  scgY(0)
{
}

QuadraticPlacer::~QuadraticPlacer(void)
{
	delete leg;
	delete scgX;
	delete scgY;
}

// Member
// -----------------------------------------------------

void QuadraticPlacer::place(void)
{
	leg = new LinearEquationGenerator(graph);
	leg->generate();

	scgX = new SparseConjugateGradient(leg->Ax, leg->Bx);
	Vector* xx = scgX->solve();

	scgY = new SparseConjugateGradient(leg->Ay, leg->By);
	Vector* xy = scgY->solve();

	// update positions
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* nodePtr = graph->getNode(i);
		if(nodePtr->isTerminal())
			continue;

		nodePtr->pos.x = (*xx)(i);
		nodePtr->pos.y = (*xy)(i);
	}
}
