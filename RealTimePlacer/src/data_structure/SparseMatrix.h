/*
 * AdjListRep.h
 *
 *  Created on: Jun 6, 2013
 *      Author: matara
 */

#ifndef ADJLISTREP_H_
#define ADJLISTREP_H_

#include <iostream>
#include <vector>
using namespace std;

#include "Vector.h"
#include "Item.h"

#define adj_list_check_boundary_conditions(x, y) \
	if(x >= rows || y >= cols) \
		throw;

class SparseMatrix
{
	friend ostream& operator<<(ostream& out, SparseMatrix &matrix)
	{
		out << "[" << matrix.getRowCount() << " x " << matrix.getRowCount() <<  "]" << std::endl;

		for(unsigned int i = 0; i < matrix.getRowCount(); ++i)
		{
			Item* currVertex = matrix.getItem(i);
			out << currVertex->key << " =>";

			while(currVertex->hasNext())
			{
				currVertex = currVertex->next;
				out << " (" << currVertex->key << ", " << currVertex->value << ")";
			}

			out << std::endl;
		}

		return out;
	}

public:
	SparseMatrix(void)
	: rows(0),
	  cols(0)
	{
	}

	SparseMatrix(unsigned int rowCount)
	: rows(rowCount),
	  cols(rowCount)
	{
		init();
	}

	SparseMatrix(unsigned int rowCount, unsigned int colCount)
	: rows(rowCount),
	  cols(colCount)
	{
		init();
	}

	SparseMatrix(const SparseMatrix& other)
	{
		copyOther(other);
	}

	virtual ~SparseMatrix(void)
	{
		clear();
	}

	SparseMatrix& operator=(const SparseMatrix& other)
	{
		if(this != &other)
		{
			clear();
			copyOther(other);
		}

		return *this;
	}

	SparseMatrix transpose(void)
	{
		SparseMatrix t(cols, rows);

		for(unsigned i = 0; i < rows; ++i)
		{
			Item* currVertex = data[i];

			while(currVertex->hasNext())
			{
				currVertex = currVertex->next;
				t(currVertex->key, i) = currVertex->value;
			}
		}

		return t;
	}

	void addItem(unsigned int vertexId, double value = 0.0)
	{
		data.push_back(new Item(vertexId, value));
		++rows;
		++cols;
	}

	inline double& operator()(const unsigned int x, const unsigned int y)
	{
		bool found = false;
		if(x >= rows)
			addItem(x);

		Item* currVertex = data[x];

		while(currVertex->hasNext() && !found)
		{
			currVertex = currVertex->next;
			if(currVertex->key == y)
				found = true;
		}

		if(found == true)
			return currVertex->value;

		// create new node with the value '0' if (x, y) doesn't exist
		currVertex->next = new Item(y);
		currVertex = currVertex->next;

		return currVertex->value;
	}

	inline int operator()(const unsigned int x, const unsigned int y) const
	{
		bool found = false;
		Item* currVertex = data[x];

		while(currVertex->hasNext() && !found)
		{
			currVertex = currVertex->next;
			if(currVertex->key == y)
				found = true;
		}

		if(found == true)
			return currVertex->value;

		return 0;
	}

	Vector operator*(const Vector& vector)
	{
		if(cols != vector.length)
			throw;

		Vector result(rows);
		for(unsigned int i = 0; i < rows; ++i)
		{
			Item* currVertex = data[i];

			while(currVertex->hasNext())
			{
				currVertex = currVertex->next;
				result(i) += currVertex->value * vector(currVertex->key);
			}
		}

		return result;
	}

	inline Item* getItem(int i) { return data[i]; }
	inline unsigned int getRowCount(void) { return rows; }
	inline unsigned int getColCount(void) { return cols; }
private:
	void copyOther(const SparseMatrix& other)
	{
		for(unsigned int i = 0; i < rows; ++i)
		{
			Item* sourceCurr = other.data[i];
			Item* destCurr = data[i];
			destCurr->copyOther(sourceCurr);

			while(sourceCurr->hasNext())
			{
				sourceCurr = sourceCurr->next;

				Item* newVertex = new Item(sourceCurr);

				destCurr->next = newVertex;
				destCurr = newVertex;
			}
		}
	}

	void clear(void)
	{
		for(unsigned int i = 0; i < rows; ++i)
		{
			Item* prevVertex = data[i];
			Item* currVertex = prevVertex->next;

			while(currVertex != 0)
			{
				prevVertex = currVertex;
				currVertex = currVertex->next;

				delete prevVertex;
			}

			delete data[i];
		}
	}

	void init(void)
	{
		for(unsigned int i = 0; i < rows; ++i)
			data.push_back(new Item(i));
	}

	unsigned int rows;
	unsigned int cols;
	vector<Item*> data;
};

#endif /* ADJLISTREP_H_ */
