/*
 * AdjMatrixRep.h
 *
 *  Created on: Jun 6, 2013
 *      Author: matara
 */

#ifndef ADJMATRIXREP_H_
#define ADJMATRIXREP_H_

#include <iostream>

#include "Vector.h"

// -----------------------------------------------------------

#define adj_matrix_for_each_elem_at_ij \
	for(unsigned int i = 0; i < rows; ++i) \
		for(unsigned int j = 0; j < cols; ++j)

// -----------------------------------------------------------

#define adj_matrix_check_bounds(x, y) \
	if(x >= rows || y >= cols) \
		throw;

#define adj_matrix_check_bounds_matrix(other) \
	if(rows != other.rows || cols != other.cols) \
		throw;

// -----------------------------------------------------------

#define adj_matrix_calculate_elem(op, elem) \
	AdjMatrixRep result(*this); \
	adj_matrix_for_each_elem_at_ij \
		result.data[i][j] = result.data[i][j] op elem; \
	return result;

#define adj_matrix_calculate_elem_self(op, elem) \
	adj_matrix_for_each_elem_at_ij \
		data[i][j] = data[i][j] op elem; \
	return *this;

// -----------------------------------------------------------

#define adj_matrix_calculate_matrix(op, other) \
	adj_matrix_check_bounds_matrix(other) \
	AdjMatrixRep result(*this); \
	adj_matrix_for_each_elem_at_ij \
		result.data[i][j] = result.data[i][j] op other.data[i][j]; \
	return result;

#define adj_matrix_calculate_matrix_self(op, other) \
	adj_matrix_check_bounds_matrix(other) \
	adj_matrix_for_each_elem_at_ij \
		data[i][j] = data[i][j] op other.data[i][j]; \
	return *this;

// -----------------------------------------------------------

class AdjMatrixRep
{
	friend std::ostream& operator<<(std::ostream& out, AdjMatrixRep &matrix)
	{
		for(unsigned int i = 0; i < matrix.rows; ++i)
		{
			for(unsigned int j = 0; j < matrix.cols; ++j)
				out << matrix.data[i][j] << " ";

			out << std::endl;
		}

		return out;
	}

	friend AdjMatrixRep operator+(int left, AdjMatrixRep& right) { return right + left;	}
	friend AdjMatrixRep operator-(int left, AdjMatrixRep& right) { return right - left;	}
	friend AdjMatrixRep operator/(int left, AdjMatrixRep& right) { return right / left;	}
	friend AdjMatrixRep operator*(int left, AdjMatrixRep& right) { return right * left;	}

public:
	AdjMatrixRep(void);
	AdjMatrixRep(unsigned int rowCount, unsigned int colCount);
	AdjMatrixRep(const AdjMatrixRep& other);
	virtual ~AdjMatrixRep(void);

	AdjMatrixRep& operator=(const AdjMatrixRep& other);

	inline int& operator()(const int x, const int y) { return data[x][y]; }
	inline int operator()(const int x, const int y) const { return data[x][y]; }

	inline AdjMatrixRep operator+(int right) const { adj_matrix_calculate_elem(+, right) }
	inline AdjMatrixRep operator-(int right) const { adj_matrix_calculate_elem(-, right) }
	inline AdjMatrixRep operator*(int right) const { adj_matrix_calculate_elem(*, right) }
	inline AdjMatrixRep operator/(int right) const { adj_matrix_calculate_elem(/, right) }
	inline AdjMatrixRep& operator+=(int right) { adj_matrix_calculate_elem_self(+, right) }
	inline AdjMatrixRep& operator-=(int right) { adj_matrix_calculate_elem_self(-, right) }
	inline AdjMatrixRep& operator/=(int right) { adj_matrix_calculate_elem_self(/, right) }
	inline AdjMatrixRep& operator*=(int right) { adj_matrix_calculate_elem_self(*, right) }

	inline AdjMatrixRep operator+(const AdjMatrixRep& right) const { adj_matrix_calculate_matrix(+, right) }
	inline AdjMatrixRep operator-(const AdjMatrixRep& right) const { adj_matrix_calculate_matrix(-, right) }
	inline AdjMatrixRep operator*(const AdjMatrixRep& right) const
	{
		if(rows != right.cols)
			throw;

		AdjMatrixRep result(rows, cols);
		adj_matrix_for_each_elem_at_ij
			for(unsigned int k = 0; k < rows; ++k)
				result(i,j) += data[i][k] * right.data[k][j];
		return result;
	}

	AdjMatrixRep& operator+=(const AdjMatrixRep& right) { adj_matrix_calculate_matrix_self(+, right) }
	AdjMatrixRep& operator-=(const AdjMatrixRep& right) { adj_matrix_calculate_matrix_self(-, right) }
	AdjMatrixRep& operator*=(const AdjMatrixRep& right)
	{
		*this = *this * right;
		return *this;
	}

	Vector operator*(const Vector& vector)
	{
		if(cols != vector.length)
			throw;

		Vector result(rows);
		adj_matrix_for_each_elem_at_ij
			result(i) += data[i][j] * vector(j);

		return result;
	}

	size_t rows;
	size_t cols;
private:
	void copyOther(const AdjMatrixRep& other);
	void clear(void);
	void init(void);

	int **data;
};

#endif /* ADJMATRIXREP_H_ */
