/*
 * Node.h
 *
 *  Created on: Jun 6, 2013
 *      Author: matara
 */

#ifndef VERTEX_H_
#define VERTEX_H_

#include <ostream>

struct Item
{
	friend std::ostream& operator<<(std::ostream& out, Item &vertex)
	{
		out << "(" << vertex.key << ", " << vertex.value <<  ") ";
		return out;
	}

	// Constructors
	// ----------------------------------------------

	Item(unsigned int key, double value = 0.0f)
	: key(key),
	  value(value),
	  next(0)
	{
	}

	Item(const Item* other)
	: key(0),
	  value(0),
	  next(0)
	{
		copyOther(other);
	}

	// Members
	// ----------------------------------------------

	inline bool hasNext(void)
	{
		return next != 0;
	}

	void copyOther(const Item *node)
	{
		key = node->key;
		value = node->value;
	}

	unsigned int key;
	double value;
	Item* next;
};


#endif /* VERTEX_H_ */
