/*
 * Vector.h
 *
 *  Created on: Jun 1, 2013
 *      Author: matara
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include <iostream>

#define calculate_for_elem(op, elem) \
	Vector result(*this); \
	for(unsigned int i = 0; i < result.length; ++i) \
		result.data[i] = result.data[i] op elem; \
	return result;

#define calculate_for_elem_self(op, elem) \
	for(unsigned int i = 0; i < length; ++i) \
		data[i] = data[i] op elem; \
	return *this;

// -----------------------------------------------------------------------

#define calculate_for_vec(op, otherVec) \
	if(length != otherVec.length) \
		throw; \
	Vector result(*this); \
	for(unsigned int i = 0; i < length; ++i) \
		result.data[i] = result.data[i] op otherVec.data[i]; \
	return result;

#define calculate_for_vec_self(op, otherVec) \
	if(length != otherVec.length) \
		throw; \
	for(unsigned int i = 0; i < length; ++i) \
		data[i] = data[i] op otherVec.data[i]; \
	return *this;

// -----------------------------------------------------------------------

// template <class T>
class Vector
{
	friend Vector operator+(int left, Vector& right) { return right + left; }
	friend Vector operator-(int left, Vector& right) { return right - left; }
	friend Vector operator/(int left, Vector& right) { return right / left; }
	friend Vector operator*(int left, Vector& right) { return right * left; }

	friend std::ostream& operator<<(std::ostream &out, Vector &vector)
	{
		out << "(" << vector.length << ")" << std::endl;

		for(unsigned int i = 0; i < vector.length; ++i)
			out << vector.data[i] << " ";
		out << std::endl;

		return out;
	}

public:
	Vector(void)
	: length(0),
	  data(0)
	{
	}

	Vector(unsigned length)
	: length(length),
	  data(0)
	{
		data = new double[length];
		for(unsigned int i = 0; i < length; ++i)
			data[i] = 0.0;
	}

	Vector(const Vector& other)
	: length(0),
	  data(0)
	{
		copyOther(other);
	}

	virtual ~Vector(void)
	{
		if(data == 0)
			return;

		delete [] data;
	}

	inline Vector& operator=(const Vector& other)
	{
		if(this != &other)
		{
			clear();
			copyOther(other);
		}

		return *this;
	}

	inline double& operator()(unsigned int index) const { return data[index]; }
	// inline int& operator()(int index) const { return data[index]; }

	inline Vector operator+(int& right) const { calculate_for_elem(+, right) }
	inline Vector operator-(int& right) const { calculate_for_elem(-, right) }
	inline Vector operator/(int& right) const { calculate_for_elem(/, right) }
	inline Vector operator*(int& right) const { calculate_for_elem(*, right) }

	inline Vector operator+(Vector &right) const { calculate_for_vec(+, right) }
	inline Vector operator-(Vector &right) const { calculate_for_vec(-, right) }
	inline Vector operator/(Vector &right) const { calculate_for_vec(/, right) }
	inline Vector operator*(Vector &right) const { calculate_for_vec(*, right) }

	Vector& operator+=(const int& right) { calculate_for_elem_self(+, right) }
	Vector& operator-=(const int& right) { calculate_for_elem_self(-, right) }
	Vector& operator/=(const int& right) { calculate_for_elem_self(/, right) }
	Vector& operator*=(const int& right) { calculate_for_elem_self(*, right) }

	Vector& operator+=(const Vector& right) { calculate_for_vec_self(+, right) }
	Vector& operator-=(const Vector& right) { calculate_for_vec_self(-, right) }
	Vector& operator/=(const Vector& right) { calculate_for_vec_self(/, right) }
	Vector& operator*=(const Vector& right) { calculate_for_vec_self(*, right) }

	size_t length;
private:

	void copyOther(const Vector& other)
	{
		length = other.length;
		data = new double[length];

		for(unsigned int i = 0; i < length; ++i)
			data[i] = other.data[i];
	}

	void clear(void)
	{
		if(data == 0)
			return;

		length = 0;
		delete [] data;
	}

	double* data;
};
#endif /* VECTOR_H_ */
