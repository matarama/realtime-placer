/*
 * Boid.h
 *
 *  Created on: Jun 12, 2013
 *      Author: matara
 */

#ifndef BOID_H_
#define BOID_H_

#include <string>

#include "../vlsi_structure/Graph.h"
#include "../vlsi_structure/Grid.h"
#include "../vlsi_structure/Board.h"

class Boid
{
public:
	Boid(Board *board);
	virtual ~Boid(void);

	void simulate(void);

	void applyCurrent(void);
	void applyFriction(void);
	void applySeparation(void);
	void applyAlignment(void); // only between members of the same flock
	void applyCohesion(void); // only between members of the same flock
	void applyBoundary(void);
	void applyTeleportation(void);

	bool isStable(void);
	void printStatistics(void);
	void printNodeLocations(void);
	std::string getLocationStr(int location);

private:
	void applyBoundaryForce(Node *node, int direction);
	bool checkStoppingCondition(void);
	Force calculateCurrentForceOnNode(Node* node);

	Board* board;
	Graph* graph;
	Grid* grid;

	// density at each cell should be lower than this value.
	double roiRatio;

	// like targeted density, lower bound density is designed to stop the simulation.
	// simulation will stop if density value for every grid-cell satisfies the condition;
	// lowerBoundDensity <= density <= targetedDensity
	// if lowerBoundDensity > targetedDensity
	// then stopping condition will be: targetedDensity <= density <= lowerBoundDensity
	// lower bound density is calculated dynamically with the following formula;
	// lowerBoundDensity = (totalCellArea) / totalArea
	// double lowerBoundDensity;

	// if targettedDensity and lowerBoundDensity is too close to each other
	// the this value is subtracted/added from/to corresponding part of the
	// equation
	// TODO: determine this value dynamically
	// If we choose the biggest cell it might introduce too much of a load balance
	// Maybe we should get the average of all the cells and use this.
	// Yeah this is the current implementation
	// double imbalanceTreshold;

	// this value is used to introduce feasibility (not sure for now) and keep the
	// load balance at the same time.
	// TODO: guarantee feasibility
	// double averageCellSize;


	// double maxMoveDistance;
	// TODO: All this is handled in the constructor so write couple comments there.
};

#endif /* BOID_H_ */
