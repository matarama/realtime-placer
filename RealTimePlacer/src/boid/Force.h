/*
 * Force.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef FORCE_H_
#define FORCE_H_

#include <cmath>
#include <ostream>
#include <string>
#include <sstream>

#include "Position.h"


const int BINARY_FORCE_DIRECTION_LEFT = 2; 	// 00000010
const int BINARY_FORCE_DIRECTION_RIGHT = 4;	// 00000100
const int BINARY_FORCE_DIRECTION_DOWN = 8;	// 00001000
const int BINARY_FORCE_DIRECTION_UP = 16;	// 00010000
const int BINARY_FORCE_Y_MASK = 24;			// 00011000
const int BINARY_FORCE_X_MASK = 6;			// 00000110

const int FORCE_DIRECTION_LEFT = 0;
const int FORCE_DIRECTION_RIGHT = 1;
const int FORCE_DIRECTION_UP = 2;
const int FORCE_DIRECTION_DOWN = 3;

// ---------------------------------------------------------------------------

#define force_calculate_mem(op, mem) \
	Force result(*this); \
	result = result op mem; \
	return result;

#define force_calculate_mem_self(op, mem) \
	*this = *this op mem; \
	return *this;

// ---------------------------------------------------------------------------

#define force_calculate_force(op, other) \
	Force result; \
	result.x = x op other.x; \
	result.y = y op other.y; \
	return result;

#define force_calculate_force_self(op, other) \
	x = x op other.x; \
	y = y op other.y; \
	return *this;

#define force_calculate_force_ptr(op, other) \
	Force result; \
	result.x = x op other->x; \
	result.y = y op other->y; \
	return result;

#define force_calculate_force_self_ptr(op, other) \
	x = x op other->x; \
	y = y op other->y; \
	return *this;

// ---------------------------------------------------------------------------

struct Force : Position
{
	/*
	friend std::ostream& operator<<(std::ostream& out, const Force& force)
	{
		out << " => " << force;
		return out;
	}
*/
	// TODO
	// friend Position operator+(const Position& pos, const Force& force) { return force + pos; }
	// friend Position operator-(const Position& pos, const Force& force) { return force - pos; }
	// friend Position operator/(const Position& pos, const Force& force) { return force / pos; }
	// friend Position operator*(const Position& pos, const Force& force) { return force * pos; }

	// ---------------------------------------------------------------------------

	Force(void)
	{
		this->x = 0.0;
		this->y = 0.0;
	}

	Force(const Position& other)
	{
		this->x = other.x;
		this->y = other.y;
	}

	Force(const Force& other)
	{
		this->x = other.x;
		this->y = other.y;
	}

	Force(double x, double y)
	{
		this->x = x;
		this->y = y;
	}

	/*
	Force operator/(const int& i) const { force_calculate_mem(/, i) }
	Force operator*(double& i) const { force_calculate_mem(*, i) }
	Force& operator/=(const int& i) { force_calculate_mem_self(/, i) }
	Force& operator*=(const int& i) { force_calculate_mem_self(*, i) }

	Force operator/(const double& d) const { force_calculate_mem(/, d) }
	Force operator*(const double& d) const { force_calculate_mem(*, d) }
	Force& operator/=(const double& d) { force_calculate_mem_self(/, d) }
	Force& operator*=(const double& d) { force_calculate_mem_self(*, d) }

	Force operator+(Force other) const { force_calculate_force(+, other) }
	Force operator-(Force other) const { force_calculate_force(-, other) }
	Force operator/(Force other) const { force_calculate_force(/, other) }
	Force operator*(Force other) const { force_calculate_force(*, other) }
	Force& operator+=(const Force other) { force_calculate_force_self(+, other) }
	Force& operator-=(const Force other) { force_calculate_force_self(-, other) }
	Force& operator/=(const Force other) { force_calculate_force_self(/, other) }
	Force& operator*=(const Force other) { force_calculate_force_self(*, other) }

	Force operator+(Force* other) const { force_calculate_force_ptr(+, other) }
	Force operator-(Force* other) const { force_calculate_force_ptr(-, other) }
	Force operator/(Force* other) const { force_calculate_force_ptr(/, other) }
	Force operator*(Force* other) const { force_calculate_force_ptr(*, other) }
	Force& operator+=(const Force* other) { force_calculate_force_self_ptr(+, other) }
	Force& operator-=(const Force* other) { force_calculate_force_self_ptr(-, other) }
	Force& operator/=(const Force* other) { force_calculate_force_self_ptr(/, other) }
	Force& operator*=(const Force* other) { force_calculate_force_self_ptr(*, other) }

	Force& operator=(const Force& other)
	{
		this->x = other.x;
		this->y = other.y;

		return *this;
	}

	Force& operator=(Force& other)
	{
		this->x = other.x;
		this->y = other.y;

		return *this;
	}
*/
	inline double getLength(void) const
	{
		return sqrt(getLengthSquared());
	}

	inline double getLengthSquared(void) const
	{
		return x * x + y * y;
	}

	inline void reset(void)
	{
		x = 0.0;
		y = 0.0;
	}

	inline Force getNegated(void)
	{
		Force f(*this);
		f.negate();
		return f;
	}

	inline void negate(void)
	{
		x = -x;
		y = -y;
	}

	inline Force getNormalized(void)
	{
		Force f(*this);
		f.normalize();
		return f;
	}

	inline void normalize(void)
	{
		double length = getLength();
		if(length == 0)
			return;

		x /= length;
		y /= length;
	}

	/**
	 *
	 */
	inline void shutDown(Force& direction)
	{
		if((x > 0 && direction.x > 0) || (x < 0 && direction.x < 0))
			x = 0.0;

		if((y > 0 && direction.y > 0) || (y < 0 && direction.y < 0))
			y = 0.0;
	}

	inline std::string getDirStr(void)
	{
		std::stringstream converter;
		converter << "[";

		if(x < 0)
			converter << -1;
		else
			converter << 1;

		converter << ",";

		if(y < 0)
			converter << -1;
		else
			converter << 1;

		converter << "]";

		return converter.str();
	}

	inline int getDirBinary(double min)
	{
		int direction = 0;
		double absX = x > 0 ? x : -x;
		double absY = y > 0 ? y : -y;


		if(absX > min)
		{
			if(x < min)
				direction = direction | BINARY_FORCE_DIRECTION_LEFT;
			else if(x > min)
				direction = direction | BINARY_FORCE_DIRECTION_RIGHT;
		}

		if(absY > min)
		{
			if(y < min)
				direction = direction | BINARY_FORCE_DIRECTION_UP;
			else if(y > min)
				direction = direction | BINARY_FORCE_DIRECTION_DOWN;
		}

		return direction;
	}
};

#endif /* FORCE_H_ */
