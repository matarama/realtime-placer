/*
 * Boid.cpp
 *
 *  Created on: Jun 12, 2013
 *      Author: matara
 */


#include <cmath>
#include <ostream>

#include "../MainConfiguration.h"

#include "../boid/Force.h"

#include "../vlsi_structure/GridCell.h"
#include "../vlsi_structure/Grid.h"
#include "../vlsi_structure/Node.h"

#include "Boid.h"

inline Force interpolate(Force f1, Force f2, double k, double l);
inline void findDirectionOf(Force& f, Position& source, Position& destination);
inline double calculateDistance(Position& source, Position& destination);
inline double calculateDistanceSquared(Position& source, Position& destination);
inline bool hitTestNode(Node* source, Node* destination);
inline bool hitTestROI(Node* source, Node* destination);
inline bool hitTest(Position& p1TopLeft, Position& p1BottomRight, Position& p2TopLeft, Position& p2BottomRight);
inline int findNodeLocation(Node* node, Board* board);
inline void translateToValidLoc(Node* node, Board* board, int& location);
inline void teleportToOpositeEdge(Node* node, Board* board, int& location);
inline Force calcForceDirForRectBasedROI(Node* source, Node* destination);
inline void findIntersectionPoints(Node* n1, Node* n2, Position& p1, Position& p2);
inline double calculateArea(const Position& p1, const Position& p2);

Boid::Boid(Board* board)
: board(board),
  graph(board->graph),
  grid(board->grid),
  roiRatio(0.0)
{
	// roiRatio = totalAreaOfBoard / totalAreaOfAllNodes
	roiRatio = (board->dimX * board->dimY) / graph->getTotalCellArea();

	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		node->calcROI(roiRatio);
	}
}

Boid::~Boid(void)
{
}

// Members
// ---------------------------------------------------------------------

void Boid::simulate(void)
{

#if BOUNDARY_RULE_USED == BOUNDARY_RULE
	applyBoundary();
// TODO: results in infinite loop for now.
#elif BOUNDARY_RULE_USED == TELEPORTATION_RULE
	applyTeleportation();
#endif

	applySeparation();
	applyCohesion();
	applyFriction();

	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* currNode = graph->getNode(i);
		currNode->calcVelocity();
		currNode->move();
	}

	std::cout << endl;

	// applyAlignment();
}

// Current rule
// -----------------------------------------------------------------------

void Boid::applyCurrent(void)
{
	grid->calculateCurrents();

	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* currNode = graph->getNode(i);

		if(currNode->isTerminal())
			continue;

		// std::cout << "ApplyCurrent::currForce before => " << currNode->currForce << std::endl;
		currNode->force += calculateCurrentForceOnNode(currNode);
		// std::cout << "ApplyCurrent::currForce after => " << currNode->currForce << std::endl;
	}
}

Force Boid::calculateCurrentForceOnNode(Node* node)
{
	Force result;

	GridCell* residenceCell = grid->findResidenceCell(node->pos);

	double kHorizontal = node->pos.x;
	while(kHorizontal - grid->cellLengthX > 0)
		kHorizontal -= grid->cellLengthX;

	std::cout << "Calculating force on " << *node << std::endl;
	std::cout << "Residence Cell: " << std::endl;
	std::cout << *residenceCell << std::endl;

	Force top = interpolate(residenceCell->topLeftForce, residenceCell->topRightForce, kHorizontal, grid->cellLengthX);
	Force bottom = interpolate(residenceCell->bottomLeftForce, residenceCell->bottomRightForce, kHorizontal, grid->cellLengthX);

	std::cout << "Top: " << top << std::endl;
	std::cout << "Bottom: " << bottom << std::endl;

	double kVertical = node->pos.y;
	while(kVertical - grid->cellLengthY > 0)
		kVertical -= grid->rows;

	result = interpolate(top, bottom, kVertical, grid->cellLengthY);

	std::cout << "Final: " << result << std::endl;

	return result;
}

// Separation rule
// -----------------------------------------------------------------------
/**
 * Each flock member has a region of influence and
 * its position is altered to avoid collision with
 * other members in the region.
 */
void Boid::applySeparation(void)
{
/*	for(unsigned int i = 0; i < graph->getTotalNetCount(); ++i)
	{
		Net* net = graph->getNet(i);

		for(unsigned int j = 0; j < net->nodes.size(); ++j)
		{
			Node* node = graph->getNode(net->nodes[j]);
			for(unsigned int k = j+1; k < net->nodes.size(); ++k)
			{
				Node* otherNode = graph->getNode(net->nodes[k]);
*/
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);

		for(unsigned int j = i + 1; j < graph->getTotalNodeCount(); ++j)
		{
			Node* otherNode = graph->getNode(j);

// TODO node weight lerini de hesaba kat
#if ROI_SHAPE_USED == ROI_CIRCLE

			// Area Model
			double distance = calculateDistance(node->pos, otherNode->pos);
			double maxRoiDistance = node->roiRadius + otherNode->roiRadius;

			// check ROI intersections and apply separation forces
			if(hitTestNode(node, otherNode))
			{
				Force fDir = otherNode->pos - node->pos;
				fDir.normalize();
				findDirectionOf(fDir, node->pos, otherNode->pos);

				Force fOtherDir = fDir.getNegated();

				Position p1, p2;
				findIntersectionPoints(node, otherNode, p1, p2);

				double totalArea = node->area + otherNode->area;

				double magn = calculateArea(p1, p2);
				double fMagn = magn * (node->area / totalArea);
				double fOtherMagn = magn * (otherNode->area / totalArea);


				Force sepFNode = fOtherDir * fMagn;
				Force sepFOtherNode = fDir * fOtherMagn;

				// std::cout << "Sep Force on Node-" << node->id << " " << sepFNode << std::endl;
				// std::cout << "Sep Force on Node-" << otherNode->id << " " << sepFOtherNode << std::endl;
				// std::cout << "Area: " << magn << " Total Area: " << totalArea << std::endl;

				// check for actual collision if there is a ROI intersection
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(sepFNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(sepFOtherNode);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += sepFNode;

					if(otherNode->isNonTerminal())
						otherNode->force += sepFOtherNode;
				}
			}

			// Distance Model
/*			double distance = calculateDistance(node->pos, otherNode->pos);
			double maxRoiDistance = node->roiRadius + otherNode->roiRadius;

			// check ROI intersections and apply separation forces
			if(distance < maxRoiDistance)
			{
				Force f = otherNode->pos - node->pos;

				// if true then there is no ROI intersection
				if(f.getLengthSquared() < MIN_FORCE)
					continue;

				f = (f / distance) * (maxRoiDistance - distance);
				findDirectionOf(f, node->pos, otherNode->pos);
				Force fNegated = f.getNegated();

				// check for actual collision if there is a ROI intersection
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fNegated);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(f);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += fNegated;

					if(otherNode->isNonTerminal())
						otherNode->force += f;
				}
			}
*/
#else // if ROI_SHAPE_USED == ROI_RECTANGLE
// TODO node weight lerini de hesaba kat
#if ROI_RECTANGLE_SEPARATION_MODEL_USED == RRSM_1

			if(hitTestROI(node, otherNode))
			{
				double distance = calculateDistance(node->pos, otherNode->pos);
				double maxROIDistance = node->halfDiagonalLength + otherNode->halfDiagonalLength;

				std::cout << "maxROIDistance: " << maxROIDistance << " distance: " << distance << std::endl;

				// calculate magnitudes
				double fMagn = maxROIDistance - distance;
				double fMagnNode = (otherNode->halfDiagonalLength / maxROIDistance) * fMagn;
				double fMagnOtherNode = (node->halfDiagonalLength / maxROIDistance) * fMagn;

				std::cout << "fMagn: " << fMagn << " fMagnNode: " << fMagnNode << " fMagnOtherNode: " << fMagnOtherNode << std::endl;

				// calculate direction
				Force fDir = calcForceDirForRectBasedROI(node, otherNode);
				Force fOtherDir = calcForceDirForRectBasedROI(otherNode, node);

				std::cout << "Node-" << node->id << " fDir: " << fDir << std::endl;
				std::cout << "Node-" << otherNode->id << " fOtherDir: " << fOtherDir << std::endl;

				// calculate force
				Force fNode = fDir * fMagnNode;
				Force fOtherNode = fOtherDir * fMagnOtherNode;

				// TODO when sudo nodes are added weight should be included
/*				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fOtherNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(fNode);
				}
				else
*/				{
					if(node->isNonTerminal())
						node->force += fNode;

					if(otherNode->isNonTerminal())
						otherNode->force += fOtherNode;
				}

				std::cout << std::endl;
			}

// TODO node weight lerini de hesaba kat
#elif ROI_RECTANGLE_SEPARATION_MODEL_USED == RRSM_2
			double fMagnNode = (otherNode->halfDiagonalLength / maxROIDistance) * fMagn;
			double fMagnOtherNode = (node->halfDiagonalLength / maxROIDistance) * fMagn;

			if(hitTestROI(node, otherNode))
			{
				Position p1;
				Position p2;
				findIntersectionPoints(node, otherNode, p1, p2);
				double distance = calculateDistance(p1, p2);

				double maxROIDistance;
				if(node->halfDiagonalLength > otherNode->halfDiagonalLength)
					maxROIDistance = otherNode->halfDiagonalLength;
				else
					maxROIDistance = node->halfDiagonalLength;

				// calculate magnitudes
				double fMagn = maxROIDistance - distance;
				double fMagnNode = (otherNode->halfDiagonalLength / maxROIDistance) * fMagn;
				double fMagnOtherNode = (node->halfDiagonalLength / maxROIDistance) * fMagn;

				// calculate direction
				Force fDir = calcForceDirForRectBasedROI(node, otherNode);
				Force fOtherDir = calcForceDirForRectBasedROI(otherNode, node);

				// calculate force
				Force fNode = fDir * fMagnNode;
				Force fOtherNode = fOtherDir * fMagnOtherNode;

				// TODO when sudo nodes are added weight should be included
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fOtherNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(fNode);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += fOtherNode;

					if(otherNode->isNonTerminal())
						otherNode->force += fNode;
				}

			}

#elif ROI_RECTANGLE_SEPARATION_MODEL_USED == RRSM_3

			if(hitTestROI(node, otherNode))
			{
				double distance = calculateDistance(node->pos, otherNode->pos);
				double maxROIDistance = node->halfDiagonalLength + otherNode->halfDiagonalLength;

				// calculate magnitudes
				double fMagn = maxROIDistance - distance;
				double fMagnNode = fMagn / 2;
				double fMagnOtherNode = fMagn / 2;

				// calculate direction
				Force fDir = calcForceDirForRectBasedROI(node, otherNode);
				Force fOtherDir = calcForceDirForRectBasedROI(otherNode, node);

				// calculate force
				Force fNode = fDir * fMagnNode;
				Force fOtherNode = fOtherDir * fMagnOtherNode;

				// TODO when sudo nodes are added weight should be included
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fOtherNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(fNode);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += fOtherNode;

					if(otherNode->isNonTerminal())
						otherNode->force += fNode;
				}

			}

#elif ROI_RECTANGLE_SEPARATION_MODEL_USED == RRSM_4

			if(hitTestROI(node, otherNode))
			{
				Position p1;
				Position p2;
				findIntersectionPoints(node, otherNode, p1, p2);
				double distance = calculateDistance(p1, p2);

				double maxROIDistance;
				if(node->halfDiagonalLength > otherNode->halfDiagonalLength)
					maxROIDistance = otherNode->halfDiagonalLength;
				else
					maxROIDistance = node->halfDiagonalLength;

				// calculate magnitudes
				double fMagn = maxROIDistance - distance;
				double fMagnNode = fMagn / 2;
				double fMagnOtherNode = fMagn / 2;

				// calculate direction
				Force fDir = calcForceDirForRectBasedROI(node, otherNode);
				Force fOtherDir = calcForceDirForRectBasedROI(otherNode, node);

				// calculate force
				Force fNode = fDir * fMagnNode;
				Force fOtherNode = fOtherDir * fMagnOtherNode;

				// TODO when sudo nodes are added weight should be included
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fOtherNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(fNode);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += fOtherNode;

					if(otherNode->isNonTerminal())
						otherNode->force += fNode;
				}

			}

#elif ROI_RECTANGLE_SEPARATION_MODEL_USED == RRSM_CIRCLE_AREA_SENSITIVE

			if(hitTestROI(node, otherNode))
			{
				Position p1;
				Position p2;

				findIntersectionPoints(node, otherNode, p1, p2);
				double distance = calculateDistance(p1, p2);
				double totalArea = node->area + otherNode->area;

				// calculate magnitudes
				// total force is divided by the cell area according to inverse ratio
				// meaning if the cell area is bigger, the force will be smaller compared to other node
				double fMagn = distance * 2;
				double fMagnNode = (otherNode->area / totalArea) * fMagn;
				double fMagnOtherNode = (node->area / totalArea) * fMagn;

				// calculate direction
				Force fDir = otherNode->pos - node->pos;
				fDir.normalize();
				findDirectionOf(fDir, node->pos, otherNode->pos);
				Force fOtherDir = fDir.getNegated();

				// calculate force
				Force fNode = fDir * fMagnNode;
				Force fOtherNode = fOtherDir * fMagnOtherNode;

				// TODO when sudo nodes are added weight should be included
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fOtherNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(fNode);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += fOtherNode;

					if(otherNode->isNonTerminal())
						otherNode->force += fNode;
				}

			}

#elif ROI_RECTANGLE_SEPARATION_MODEL_USED == RRSM_CIRCLE_HOMOGENOUS

			if(hitTestROI(node, otherNode))
			{
				Position p1;
				Position p2;

				findIntersectionPoints(node, otherNode, p1, p2);
				double distance = calculateDistance(p1, p2);
				double totalArea = node->area + otherNode->area;

				// calculate magnitudes
				// total force is divided by the cell area according to inverse ratio
				// meaning if the cell area is bigger, the force will be smaller compared to other node
				double fMagn = distance * 2;
				double fMagnNode = fMagn / 2;
				double fMagnOtherNode = fMagn / 2;

				// calculate direction
				Force fDir = otherNode->pos - node->pos;
				fDir.normalize();
				findDirectionOf(fDir, node->pos, otherNode->pos);
				Force fOtherDir = fDir.getNegated();

				// calculate force
				Force fNode = fDir * fMagnNode;
				Force fOtherNode = fOtherDir * fMagnOtherNode;

				// TODO when sudo nodes are added weight should be included
				if(hitTestNode(node, otherNode))
				{
					// TODO when sudo nodes are added weight should be included
					if(node->isNonTerminal())
						node->addPermanentVelocity(fOtherNode);

					if(otherNode->isNonTerminal())
						otherNode->addPermanentVelocity(fNode);
				}
				else
				{
					if(node->isNonTerminal())
						node->force += fOtherNode;

					if(otherNode->isNonTerminal())
						otherNode->force += fNode;
				}

			}

#endif

#endif

			}
		}
//	}
}

// Cohesion rule
// -----------------------------------------------------------------------
/**
 * Each flock member is drifted towards the average position of
 * flock mates in its region of influence.
 */
void Boid::applyCohesion(void)
{
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		if(node->isTerminal())
			continue;

		Position centerOfGravity;
		int totalWeight = 0;

		for(unsigned int j = 0; j < node->nets.size(); ++j)
		{
			int netId = node->getNetId(j);
			Net* net = graph->getNet(netId);

			for(unsigned int k = 0; k < net->nodes.size(); ++k)
			{
				unsigned friendId = net->nodes[k];
				if(friendId == node->id)
					continue;

				Node* friendNode = graph->getNode(friendId);

				centerOfGravity += friendNode->pos * friendNode->weight;
				totalWeight += friendNode->weight;
			}
		}

		centerOfGravity /= totalWeight;

		// destination: center of gravity
		// source: node->pos
		Force cohesionForce = centerOfGravity - node->pos;
		findDirectionOf(cohesionForce, node->pos, centerOfGravity);
		cohesionForce *= totalWeight;

		node->force += cohesionForce;

		// std::cout << "Force: " << node->force << std::endl;
	}
}

// Alignment rule
// -----------------------------------------------------------------------
/**
 * The direction that each flock member moves
 * is altered in order to match other flock members
 * in the same region of influence.
 */
void Boid::applyAlignment(void)
{
	// calculate average velocity direction per net.
	Force totalVNet[graph->getTotalNetCount()];
	for(unsigned int i = 0; i < graph->getTotalNetCount(); ++i)
	{
		Net* net = graph->getNet(i);

		Force totalV;
		for(unsigned int j = 0; j < net->nodes.size(); ++j)
		{
			int nodeId = net->nodes[j];
			Node* node = graph->getNode(nodeId);

			// TODO should we include permanentVelocity
			totalV += node->velocity; // + node->permanentVelocity;
		}

		totalVNet[i] = totalV;
	}

	// apply alignment to each node
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		if(node->isTerminal())
			continue;

		Force avgV;
		for(unsigned int j = 0; j < node->nets.size(); ++j)
		{
			int netId = node->nets[j];
			Net* net = graph->getNet(netId);

			// TODO which one is this better ?!
			avgV += totalVNet[netId] / net->nodes.size();
			//avgV += (totalVNet[netId] - node->velocity) / (net->nodes.size() - 1);
		}

		avgV = avgV / node->nets.size();

		node->velocity += avgV;
	}
}

// Boundary Rule
// -----------------------------------------------------------------------
/**
 * To prevent nodes from going off the board.
 */
void Boid::applyBoundary(void)
{
	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		if(node->isTerminal())
			continue;

		int location = findNodeLocation(node, board);
		if(location == BOARD_INSIDE)
			continue;

		translateToValidLoc(node, board, location);
	}
}

// Teleportation (No Boundary) Rule
// -----------------------------------------------------------------------
/**
 * Teleports the nodes to opposite edge
 */
void Boid::applyTeleportation(void)
{
	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		if(node->isTerminal())
			continue;

		int location = findNodeLocation(node, board);
		if(location == BOARD_INSIDE)
			continue;

		teleportToOpositeEdge(node, board, location);
	}
}


// Friction rule
// -----------------------------------------------------------------------
/*
 * Apply additional force that is originated from air/water density.
 * It's direction is the opposite of node's velocity and calculated by the following formula;
 *
 * R = 1/2 x (D x p x A x v^2)
 * where;
 * R: resistance
 * D: resistance multiplier. For circular objects it is 0.5 and can go up to 2.
 * p: air/water density
 * A: surface area of the node that is perpendicular to the movement
 * v: velocity of the object
 */
void Boid::applyFriction(void)
{
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		if(node->isTerminal())
			continue;

		node->calcResistance();
		node->force += node->resistance;

		// std::cout << "Resistance: " << node->resistance << std::endl;
	}
}

// -----------------------------------------------------------------------

bool Boid::isStable(void)
{
	bool isStable = true;

	for(unsigned i = 0; i < graph->getTotalNodeCount() && isStable; ++i)
	{
		Node* node = graph->getNode(i);
		isStable = node->isStable();
	}

	return isStable;
}

std::string Boid::getLocationStr(int location)
{
	switch(location)
	{
		case BOARD_EAST:
			return "east";
		case BOARD_WEST:
			return "west";
		case BOARD_NORTH:
			return "north";
		case BOARD_SOUTH:
			return "south";
		case BOARD_NORTH_EAST:
			return "north east";
		case BOARD_NORTH_WEST:
			return "north west";
		case BOARD_SOUTH_EAST:
			return "south east";
		case BOARD_SOUTH_WEST:
			return "south west";
		default:
			return "inside board";
	}
}

void Boid::printStatistics(void)
{
	std::cout << "Printing BOID statistics..." << std::endl;
	std::cout << "Total Cell Area = " << graph->getTotalCellArea() << std::endl;
	std::cout << "Board Area = " << board->dimX * board->dimY << std::endl;
	std::cout << std::endl;
}

void Boid::printNodeLocations(void)
{
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);
		int location = findNodeLocation(node, board);
		string locationStr = getLocationStr(location);

		std::cout << "Node-" << i << " " << locationStr << std::endl;
	}

	std::cout << "------------------------------" << std::endl;
}



// Private Members
// ----------------------------------------------------------------------

bool Boid::checkStoppingCondition(void)
{
	grid_iterate_all_cells_ij_ptr
	{
		GridCell* currCell = (*grid)(i, j);
		if(currCell->isActive())
			return false;
	}

	return true;
}

// Helpers
// ------------------------------------------------------------------------

inline Force interpolate(Force f1, Force f2, double k, double l)
{
	Force result = f1 * ((l - k) / l);
	result += f2 * (k / l);
	return result;
}

inline void findDirectionOf(Force& f, Position& source, Position& destination)
{
	// Consider current node position is at the center of the coordinate system (0, 0)
	if(destination.x >= source.x)
		f.x = abs(f.x);
	else
		f.x = -abs(f.x);

	if(destination.y >= source.y)
		f.y = abs(f.y);
	else
		f.y = -abs(f.y);
}

inline double calculateDistance(Position& source, Position& destination)
{
	return sqrt((destination.x - source.x) * (destination.x - source.x) +
			(destination.y - source.y) * (destination.y - source.y));
}

inline double calculateDistanceSquared(Position& source, Position& destination)
{
	return (destination.x - source.x) * (destination.x - source.x) +
			(destination.y - source.y) * (destination.y - source.y);
}

inline bool hitTestNode(Node* source, Node* destination)
{
	Position sourceTopLeft = source->calcTopLeftPos();
	Position sourceBottomRight = source->calcBottomRightPos();
	Position destinationTopLeft = destination->calcTopLeftPos();
	Position destinationBottomRight = destination->calcBottomRightPos();

	return hitTest(sourceTopLeft, sourceBottomRight, destinationTopLeft, destinationBottomRight);
}

inline bool hitTestROI(Node* source, Node* destination)
{
	Position sourceTopLeft = source->calcROITopLeftPos();
	Position sourceBottomRight = source->calcROIBottomRightPos();
	Position destinationTopLeft = destination->calcROITopLeftPos();
	Position destinationBottomRight = destination->calcROIBottomRightPos();

	return hitTest(sourceTopLeft, sourceBottomRight, destinationTopLeft, destinationBottomRight);
}

inline bool hitTest(Position& p1TopLeft, Position& p1BottomRight, Position& p2TopLeft, Position& p2BottomRight)
{
	if(p1BottomRight.x >= p2TopLeft.x &&
		p1TopLeft.x <= p2BottomRight.x &&
		p1BottomRight.y >= p2TopLeft.y &&
		p1TopLeft.y <= p2BottomRight.y)
	{
		return true;
	}

	return false;
}

// TODO BOARD_LOCATION degerleri bit olarak ifade edilince değişmesi lazım bu metodun.
inline int findNodeLocation(Node* node, Board* board)
{
	Position nodeTopLeft = node->calcTopLeftPos();
	Position nodeBottomRight = node->calcBottomRightPos();

	if(nodeTopLeft.x < 0.0 && nodeTopLeft.y < 0.0)
		return BOARD_NORTH_EAST;
	else if(nodeTopLeft.x < 0.0 && nodeBottomRight.y > board->dimY)
		return BOARD_SOUTH_EAST;
	else if(nodeBottomRight.x > board->dimX && nodeBottomRight.y > board->dimY)
		return BOARD_SOUTH_WEST;
	else if(nodeBottomRight.x > board->dimX && nodeTopLeft.y < 0.0)
		return BOARD_NORTH_WEST;
	else if(nodeTopLeft.x < 0.0)
		return BOARD_EAST;
	else if(nodeTopLeft.y < 0.0)
		return BOARD_NORTH;
	else if(nodeBottomRight.x > board->dimX)
		return BOARD_WEST;
	else if(nodeBottomRight.y > board->dimY)
		return BOARD_SOUTH;

	return BOARD_INSIDE;
}

// TODO BOARD_LOCATION degerleri bit olarak ifade edilince değişmesi lazım bu metodun.
/**
 * Translates the node to a valid location inside the board.
 * While translating node to the smallest distance valid location
 * inside board, its velocity and forces are changed.
 */
inline void translateToValidLoc(Node* node, Board* board, int& location)
{
	if(location == BOARD_NORTH)
	{
		node->pos.y = 0.0 + node->dimY / 2;
		if(node->velocity.y < 0.0)
			node->velocity.y = 0.0;
		if(node->permanentVelocity.y < 0.0)
			node->permanentVelocity.y = 0.0;
		if(node->force.y < 0.0)
			node->force.y = 0.0;
	}
	else if(location == BOARD_SOUTH)
	{
		node->pos.y = board->dimY - node->dimY / 2;
		if(node->velocity.y > 0.0)
			node->velocity.y = 0.0;
		if(node->permanentVelocity.y > 0.0)
			node->permanentVelocity.y = 0.0;
		if(node->force.y > 0.0)
			node->force.y = 0.0;
	}
	else if(location == BOARD_EAST)
	{
		node->pos.x = 0.0 + node->dimX / 2;
		if(node->velocity.x < 0.0)
			node->velocity.x = 0.0;
		if(node->permanentVelocity.x < 0.0)
			node->permanentVelocity.x = 0.0;
		if(node->force.x < 0.0)
			node->force.x = 0.0;
	}
	else if(location == BOARD_WEST)
	{
		node->pos.x = board->dimX - node->dimX / 2;
		if(node->velocity.x > 0.0)
			node->velocity.x = 0.0;
		if(node->permanentVelocity.x > 0.0)
			node->permanentVelocity.x = 0.0;
		if(node->force.x > 0)
			node->force.x = 0.0;
	}
	else if(location == BOARD_NORTH_EAST)
	{
		node->pos.x = 0.0 + node->dimX / 2;
		node->pos.y = 0.0 + node->dimY / 2;
		if(node->velocity.x < 0.0)
			node->velocity.x = 0.0;
		if(node->permanentVelocity.x < 0.0)
			node->permanentVelocity.x = 0.0;
		if(node->velocity.y < 0.0)
			node->velocity.y = 0.0;
		if(node->permanentVelocity.y < 0.0)
			node->permanentVelocity.y = 0.0;
		if(node->force.x < 0.0)
			node->force.x = 0.0;
		if(node->force.y < 0.0)
			node->force.y = 0.0;
	}
	else if(location == BOARD_NORTH_WEST)
	{
		node->pos.x = board->dimX - node->dimX / 2;
		node->pos.y = 0.0 + node->dimY / 2;
		if(node->velocity.x > 0.0)
			node->velocity.x = 0.0;
		if(node->permanentVelocity.x > 0.0)
			node->permanentVelocity.x = 0.0;
		if(node->velocity.y > 0.0)
			node->velocity.y = 0.0;
		if(node->permanentVelocity.y > 0.0)
			node->permanentVelocity.y = 0.0;
		if(node->force.x > 0.0)
			node->force.x = 0.0;
		if(node->force.y > 0.0)
			node->force.y = 0.0;
	}
	else if(location == BOARD_SOUTH_EAST)
	{
		node->pos.x = 0.0 + node->dimX / 2;
		node->pos.y = board->dimY - node->dimY / 2;
		if(node->velocity.x < 0.0)
			node->velocity.x = 0.0;
		if(node->permanentVelocity.x < 0.0)
			node->permanentVelocity.x = 0.0;
		if(node->velocity.y > 0.0)
			node->velocity.y = 0.0;
		if(node->permanentVelocity.y > 0.0)
			node->permanentVelocity.y = 0.0;
		if(node->force.x < 0.0)
			node->force.x = 0.0;
		if(node->force.y > 0.0)
			node->force.y = 0.0;
	}
	else if(location == BOARD_SOUTH_WEST)
	{
		node->pos.x = board->dimX - node->dimX / 2;
		node->pos.y = board->dimY - node->dimY / 2;
		if(node->velocity.x > 0.0)
			node->velocity.x = 0.0;
		if(node->permanentVelocity.x > 0.0)
			node->permanentVelocity.x = 0.0;
		if(node->velocity.y > 0.0)
			node->velocity.y = 0.0;
		if(node->permanentVelocity.y > 0.0)
			node->permanentVelocity.y = 0.0;
		if(node->force.x > 0.0)
			node->force.x = 0.0;
		if(node->force.y > 0.0)
			node->force.y = 0.0;
	}
}

inline void teleportToOpositeEdge(Node* node, Board* board, int& location)
{
	if(location == BOARD_NORTH)
	{
		node->pos.y = board->dimY - node->dimY / 2;
	}
	else if(location == BOARD_SOUTH)
	{
		node->pos.y = 0.0 + node->dimY / 2;
	}
	else if(location == BOARD_EAST)
	{
		node->pos.x = board->dimX - node->dimX / 2;
	}
	else if(location == BOARD_WEST)
	{
		node->pos.x = 0.0 + node->dimX / 2;
	}
	else if(location == BOARD_NORTH_EAST)
	{
		node->pos.x = board->dimX - node->dimX / 2;
		node->pos.y = board->dimY - node->dimY / 2;
	}
	else if(location == BOARD_NORTH_WEST)
	{
		node->pos.x = 0.0 + node->dimX / 2;
		node->pos.y = board->dimY - node->dimY / 2;
	}
	else if(location == BOARD_SOUTH_EAST)
	{
		node->pos.x = board->dimX - node->dimX / 2;
		node->pos.y = 0.0 + node->dimY / 2;
	}
	else if(location == BOARD_SOUTH_WEST)
	{
		node->pos.x = 0.0 + node->dimX / 2;
		node->pos.y = 0.0 + node->dimY / 2;
	}
}

inline Force calcForceDirForRectBasedROI(Node* source, Node* destination)
{
	Force ret;

	Position topLeft = destination->calcTopLeftPos();
	Position bottomRight = destination->calcBottomRightPos();

	double d1x = abs(topLeft.x - source->pos.x);
	double d2x = abs(bottomRight.x - source->pos.x);

	ret.x = (-1 * d2x + 1 * d1x) / (d1x + d2x);

	double d1y = abs(topLeft.y - source->pos.y);
	double d2y = abs(bottomRight.y - source->pos.y);

	ret.y = (-1 * d2y + 1 * d1y) / (d1y + d2y);

	// std::cout << "Direction of force: " << ret << std::endl;

	return ret;
}

inline void findIntersectionPoints(Node* n1, Node* n2, Position& p1, Position& p2)
{
	Position n1TopLeft = n1->calcTopLeftPos();
	Position n1BottomRight = n1->calcBottomRightPos();
	Position n2TopLeft = n2->calcTopLeftPos();
	Position n2BottomRight = n2->calcBottomRightPos();

	if(n1TopLeft.x > n2TopLeft.x)
	{
		if(n1BottomRight.x > n2BottomRight.x)
		{
			p1.x = n1TopLeft.x;
			p2.x = n2BottomRight.x;
		}
		else
		{
			p1.x = n1TopLeft.x;
			p2.x = n1BottomRight.x;
		}
	}
	else // if(n2TopLeft.x > n1TopLeft.x)
	{
		if(n2BottomRight.x > n1BottomRight.x)
		{
			p1.x = n2TopLeft.x;
			p2.x = n1BottomRight.x;
		}
		else
		{
			p1.x = n2TopLeft.x;
			p2.x = n2BottomRight.x;
		}
	}

	if(n1TopLeft.y > n2TopLeft.y)
	{
		if(n1BottomRight.y > n2BottomRight.y)
		{
			p1.y = n2BottomRight.y;
			p2.y = n1TopLeft.y;
		}
		else
		{
			p1.y = n1TopLeft.y;
			p2.y = n1BottomRight.y;
		}
	}
	else // if(n2TopLeft.y > n1TopLeft.y)
	{
		if(n2BottomRight.y > n1BottomRight.y)
		{
			p1.y = n2TopLeft.y;
			p2.y = n1BottomRight.y;
		}
		else
		{
			p1.y = n2TopLeft.y;
			p2.y = n2BottomRight.y;
		}
	}
}

inline double calculateArea(const Position& p1, const Position& p2)
{
	return abs(p1.x - p2.x) * abs(p1.y - p2.y);
}
