/*
 * Position.h
 *
 *  Created on: Jun 8, 2013
 *      Author: matara
 */

#ifndef POSITION_H_
#define POSITION_H_

#include <cmath>
#include <ostream>

// ---------------------------------------------------------------------------

#define position_calculate_mem(op, mem) \
	Position result(*this); \
	result.x = result.x op mem; \
	result.y = result.y op mem; \
	return result;

#define position_calculate_mem_self(op, mem) \
	x = x op mem; \
	y = y op mem; \
	return *this;

// ---------------------------------------------------------------------------

#define position_calculate_position_ptr(op, other) \
	Position result; \
	result.x = x op other->x; \
	result.y = y op other->y; \
	return result;

#define position_calculate_position(op, other) \
	Position result; \
	result.x = x op other.x; \
	result.y = y op other.y; \
	return result;

#define position_calculate_position_self(op, other) \
	x = x op other.x; \
	y = y op other.y; \
	return *this;

// ---------------------------------------------------------------------------


struct Position
{
	friend std::ostream& operator<<(std::ostream& out, Position& pos)
	{
		out << "[" << pos.x << ", " << pos.y << "]";
		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Position& pos)
	{
		out << "[" << pos.x << ", " << pos.y << "]";
		return out;
	}

	Position(double x = 0.0, double y = 0.0)
	: x(x),
	  y(y)
	{
	}

	Position(const Position& other)
	: x(other.x),
	  y(other.y)
	{
	}

	Position& operator=(const Position& other)
	{
		x = other.x;
		y = other.y;

		return *this;
	}

	inline double calculateDistanceSquared(const Position& other)
	{
		return (x - other.x) * (x - other.x) + (y - other.y) * (y - other.y);
	}

	inline double calculateDistance(const Position& other)
	{
		return sqrt(calculateDistanceSquared(other));
	}

	inline double getLengthSquared(void) const
	{
		return x * x + y * y;
	}

	inline Position operator/(const int& i) const { position_calculate_mem(/, i) }
	inline Position operator*(const int& i) const { position_calculate_mem(*, i) }
	inline Position& operator/=(const int& i) { position_calculate_mem_self(/, i) }
	inline Position& operator*=(const int& i) { position_calculate_mem_self(*, i) };

	inline Position operator/(double& d) const { position_calculate_mem(/, d) }
	inline Position operator*(double d) const { position_calculate_mem(*, d) }
	inline Position& operator/=(const double& d) { position_calculate_mem_self(/, d) }
	inline Position& operator*=(const double& d) { position_calculate_mem_self(*, d) }

	inline Position operator+(Position other) const { position_calculate_position(+, other) }
	inline Position operator-(Position other) const { position_calculate_position(-, other) }
	inline Position operator/(Position other) const { position_calculate_position(/, other) }
	inline Position operator*(Position other) const { position_calculate_position(*, other) }
	inline Position operator+(Position* other) const { position_calculate_position_ptr(+, other) }
	inline Position operator-(Position* other) const { position_calculate_position_ptr(-, other) }
	inline Position operator/(Position* other) const { position_calculate_position_ptr(/, other) }
	inline Position operator*(Position* other) const { position_calculate_position_ptr(*, other) }
	inline Position& operator+=(const Position& other) { position_calculate_position_self(+, other) }
	inline Position& operator-=(const Position& other) { position_calculate_position_self(-, other) }
	inline Position& operator/=(const Position& other) { position_calculate_position_self(/, other) }
	inline Position& operator*=(const Position& other) { position_calculate_position_self(*, other) }

	double x;
	double y;
};

#endif /* POSITION_H_ */
