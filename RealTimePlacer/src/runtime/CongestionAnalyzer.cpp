/*
 * CongestionAnalyzer.cpp
 *
 *  Created on: Jul 23, 2013
 *      Author: matara
 */

#include <cmath>

#include "CongestionAnalyzer.h"

inline void findIntersectionPoints(Node* n1, Node* n2, Position& p1, Position& p2);
inline bool hitTest(Position& p1TopLeft, Position& p1BottomRight, Position& p2TopLeft, Position& p2BottomRight);
inline bool hitTestNode(Node* source, Node* destination);

CongestionAnalyzer::CongestionAnalyzer(Graph* graph)
: graph(graph),
  ratio(0.0),
  area(0.0)
{
}

CongestionAnalyzer::~CongestionAnalyzer(void)
{
	// Do nothing
}

void CongestionAnalyzer::analyze(void)
{
	area = 0.0;

	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* n1 = graph->getNode(i);

		for(unsigned int j = i + 1; j < graph->getTotalNodeCount(); ++j)
		{
			Node* n2 = graph->getNode(j);

			if(hitTestNode(n1, n2))
			{
				Position p1;
				Position p2;
				findIntersectionPoints(n1, n2, p1, p2);

				area += abs(p1.x - p2.x) * abs(p1.y - p2.y);
			}
		}
	}

	ratio = area / graph->getTotalCellArea();
}

double CongestionAnalyzer::getCongestionRatio(void)
{
	return ratio;
}

double CongestionAnalyzer::getCongestedArea(void)
{
	return area;
}

// Helper functions
// ----------------------------------------------------------------------------

inline void findIntersectionPoints(Node* n1, Node* n2, Position& p1, Position& p2)
{
	Position n1TopLeft = n1->calcTopLeftPos();
	Position n1BottomRight = n1->calcBottomRightPos();
	Position n2TopLeft = n2->calcTopLeftPos();
	Position n2BottomRight = n2->calcBottomRightPos();

	if(n1TopLeft.x > n2TopLeft.x)
	{
		if(n1BottomRight.x > n2BottomRight.x)
		{
			p1.x = n1TopLeft.x;
			p2.x = n2BottomRight.x;
		}
		else
		{
			p1.x = n1TopLeft.x;
			p2.x = n1BottomRight.x;
		}
	}
	else // if(n2TopLeft.x > n1TopLeft.x)
	{
		if(n2BottomRight.x > n1BottomRight.x)
		{
			p1.x = n2TopLeft.x;
			p2.x = n1BottomRight.x;
		}
		else
		{
			p1.x = n2TopLeft.x;
			p2.x = n2BottomRight.x;
		}
	}

	if(n1TopLeft.y > n2TopLeft.y)
	{
		if(n1BottomRight.y > n2BottomRight.y)
		{
			p1.y = n2BottomRight.y;
			p2.y = n1TopLeft.y;
		}
		else
		{
			p1.y = n1TopLeft.y;
			p2.y = n1BottomRight.y;
		}
	}
	else // if(n2TopLeft.y > n1TopLeft.y)
	{
		if(n2BottomRight.y > n1BottomRight.y)
		{
			p1.y = n2TopLeft.y;
			p2.y = n1BottomRight.y;
		}
		else
		{
			p1.y = n2TopLeft.y;
			p2.y = n2BottomRight.y;
		}
	}
}

inline bool hitTestNode(Node* source, Node* destination)
{
	Position sourceTopLeft = source->calcTopLeftPos();
	Position sourceBottomRight = source->calcBottomRightPos();
	Position destinationTopLeft = destination->calcTopLeftPos();
	Position destinationBottomRight = destination->calcBottomRightPos();

	return hitTest(sourceTopLeft, sourceBottomRight, destinationTopLeft, destinationBottomRight);
}

inline bool hitTest(Position& p1TopLeft, Position& p1BottomRight, Position& p2TopLeft, Position& p2BottomRight)
{
	if(p1BottomRight.x >= p2TopLeft.x &&
		p1TopLeft.x <= p2BottomRight.x &&
		p1BottomRight.y >= p2TopLeft.y &&
		p1TopLeft.y <= p2BottomRight.y)
	{
		return true;
	}

	return false;
}
