/*
 * WireLengthCalculator.cpp
 *
 *  Created on: Jul 23, 2013
 *      Author: matara
 */

#include "WireLengthCalculator.h"


inline double calculateDistance(const Position& source, const Position& destination);
inline void findMinMax(Graph* graph, Net* net, double& minX, double& maxX, double& minY, double& maxY);

WireLengthCalculator::WireLengthCalculator(Graph* graph)
: graph(graph)
{
}

WireLengthCalculator::~WireLengthCalculator()
{
	// Do nothing
}

float WireLengthCalculator::calculateDiagonalWL(void)
{
	float totalWL = 0.0f;

	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* n1 = graph->getNode(i);

		for(unsigned j = i + 1; j < graph->getTotalNodeCount(); ++j)
		{
			Node* n2 = graph->getNode(j);
			double distance = calculateDistance(n1->pos, n2->pos);

			totalWL += distance;
		}
	}

	return totalWL;
}

float WireLengthCalculator::calculateHalfPerimeterWL(void)
{
	float HPWL = 0.0f;

	for(unsigned i = 0; i < graph->getTotalNetCount(); ++i)
	{
		Net* net = graph->getNet(i);

		double minX, maxX, minY, maxY;
		findMinMax(graph, net, minX, maxX, minY, maxY);

		double netHPWL = maxX - minX + maxY - minY;
		std::cout << "Net-" << net->id << " hpwl: " << netHPWL << std::endl;

		HPWL += netHPWL;
	}

	std::cout << std::endl;

	return HPWL;
}

// Some helper functions

inline double calculateDistance(const Position& source, const Position& destination)
{
	return sqrt((destination.x - source.x) * (destination.x - source.x) +
			(destination.y - source.y) * (destination.y - source.y));
}

inline void findMinMax(Graph* graph, Net* net, double& minX, double& maxX, double& minY, double& maxY)
{
	unsigned int nodeId = net->nodes[0];
	Node* node = graph->getNode(nodeId);
	minX = node->pos.x;
	maxX = node->pos.x;
	minY = node->pos.y;
	maxY = node->pos.y;

	for(unsigned i = 1; i < net->nodes.size(); ++i)
	{
		nodeId = net->nodes[i];
		node = graph->getNode(nodeId);

		if(minX > node->pos.x)
			minX = node->pos.x;
		else if(maxX < node->pos.x)
			maxX = node->pos.x;

		if(minY > node->pos.y)
			minY = node->pos.y;
		else if(maxY < node->pos.y)
			maxY = node->pos.y;
	}
}
