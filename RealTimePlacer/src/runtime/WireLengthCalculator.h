/*
 * WireLengthCalculator.h
 *
 *  Created on: Jul 23, 2013
 *      Author: matara
 */

#ifndef WIRELENGTHCALCULATOR_H_
#define WIRELENGTHCALCULATOR_H_

#include "../vlsi_structure/Graph.h"

class WireLengthCalculator
{
public:
	WireLengthCalculator(Graph* graph);
	virtual ~WireLengthCalculator(void);

	float calculateDiagonalWL(void);
	float calculateHalfPerimeterWL(void);

private:
	Graph* graph;
};

#endif /* WIRELENGTHCALCULATOR_H_ */
