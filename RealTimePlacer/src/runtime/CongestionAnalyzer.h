/*
 * CongestionAnalyzer.h
 *
 *  Created on: Jul 23, 2013
 *      Author: matara
 */

#ifndef CONGESTIONANALYZER_H_
#define CONGESTIONANALYZER_H_

#include "../vlsi_structure/Graph.h"

class CongestionAnalyzer
{
public:
	CongestionAnalyzer(Graph* graph);
	virtual ~CongestionAnalyzer(void);

	void analyze(void);
	double getCongestionRatio(void);
	double getCongestedArea(void);

private:
	Graph* graph;
	double ratio;
	double area;
};

#endif /* CONGESTIONANALYZER_H_ */
