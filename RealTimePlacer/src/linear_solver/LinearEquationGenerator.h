/*
 * LinearEquationGenerator.h
 *
 *  Created on: Jun 10, 2013
 *      Author: matara
 */

#ifndef LINEAREQUATIONGENERATOR_H_
#define LINEAREQUATIONGENERATOR_H_

#include "../data_structure/SparseMatrix.h"
#include "../data_structure/Vector.h"
#include "../vlsi_structure/Graph.h"

class LinearEquationGenerator
{
public:
	LinearEquationGenerator(void);
	LinearEquationGenerator(Graph* graph);
	virtual ~LinearEquationGenerator(void);

	void generate(void);

	Graph* graph;
	SparseMatrix* Ax;
	Vector* Bx;
	SparseMatrix* Ay;
	Vector* By;
private:
	void generateX(void);
	void generateY(void);
};

#endif /* LINEAREQUATIONGENERATOR_H_ */
