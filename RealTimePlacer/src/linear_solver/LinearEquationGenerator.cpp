/*
 * LinearEquationGenerator.cpp
 *
 *  Created on: Jun 10, 2013
 *      Author: matara
 */

#include "LinearEquationGenerator.h"

LinearEquationGenerator::LinearEquationGenerator(void)
: graph(0),
  Ax(0),
  Bx(0),
  Ay(0),
  By(0)
{
}

LinearEquationGenerator::LinearEquationGenerator(Graph* graph)
: graph(graph),
  Ax(0),
  Bx(0),
  Ay(0),
  By(0)
{
	int size = graph->getTotalNodeCount();
	Ax = new SparseMatrix(size);
	Bx = new Vector(size);
	Ay = new SparseMatrix(size);
	By = new Vector(size);
}

LinearEquationGenerator::~LinearEquationGenerator(void)
{
	delete Ax;
	delete Bx;
	delete Ay;
	delete By;
}

// member
// -------------------------------------------------------------------

void LinearEquationGenerator::generate(void)
{
	generateX();
	generateY();
}

// private members
// -------------------------------------------------------------------

void LinearEquationGenerator::generateX(void)
{
	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node *currNode = graph->getNode(i);
		if(currNode->isTerminal())
			continue;

		Item* itemPtr = graph->getItem(i);
		while(itemPtr->hasNext())
		{
			itemPtr = itemPtr->next;
			unsigned neighborNodeId = itemPtr->key;
			double edgeWeight = itemPtr->value;
			Node* neighborNode = graph->getNode(neighborNodeId);

			(*Ax)(i, i) += 2 * edgeWeight;
			if(neighborNode->isTerminal())
				(*Bx)(i) += 2 * neighborNode->pos.x * edgeWeight;
			else
				(*Ax)(i, neighborNodeId) += -2 * edgeWeight;
		}
	}
}

void LinearEquationGenerator::generateY(void)
{
	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node *currNode = graph->getNode(i);
		if(currNode->isTerminal())
			continue;

		Item* itemPtr = graph->getItem(i);
		while(itemPtr->hasNext())
		{
			itemPtr = itemPtr->next;
			unsigned neighborNodeId = itemPtr->key;
			double edgeWeight = itemPtr->value;
			Node* neighborNode = graph->getNode(neighborNodeId);

			(*Ay)(i, i) += 2 * edgeWeight;
			if(neighborNode->isTerminal())
				(*By)(i) += 2 * neighborNode->pos.y * edgeWeight;
			else
				(*Ay)(i, neighborNodeId) += -2 * edgeWeight;
		}
	}
}
