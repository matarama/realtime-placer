/*
 * SparseConjugateGradient.cpp
 *
 *  Created on: Jan 10, 2013
 *      Author: Matara Ma Sukoy
 */

#include <math.h>
#include <iostream>

#include "SparseConjugateGradient.h"

SparseConjugateGradient::SparseConjugateGradient()
: A(NULL),
  x(NULL),
  B(NULL),
  iterMax(10000),
  tol(0.001)
{
}

SparseConjugateGradient::SparseConjugateGradient(SparseMatrix *A, Vector *B)
: A(A),
  x(0),
  B(B),
  iterMax(10000),
  tol(0.001)
{
	x = new Vector(A->getColCount());
}

SparseConjugateGradient::~SparseConjugateGradient()
{
	delete x;
}

Vector* SparseConjugateGradient::solve(bool isTransposed)
{
	double ak;
	double akden;
	double bk;
	double bkden = 1.0;
	double bnrm;
	double bknum;

	double err;

	unsigned n = B->length;
	Vector p(n);
	Vector pp(n);
	Vector r(n);
	Vector rr(n);
	Vector z(n);
	Vector zz(n);

	int iter = 0;
	r = atimes(x, false);
	for(unsigned i = 0; i < r.length; ++i)
	{
		r(i) = (*B)(i) - r(i);
		rr(i) = r(i);
	}

	bnrm = snorm(B);
	z = asolve(&r);

	while(iter < iterMax)
	{
		++iter;
		zz = asolve(&rr);

		bknum = 0.0;
		for(unsigned j = 0; j < n; ++j)
			bknum += z(j) * rr(j);

		if(iter == 1)
		{
			for(unsigned j = 0; j < n; ++j)
			{
				p(j) = z(j);
				pp(j) = zz(j);
			}
		}
		else
		{
			bk = bknum / bkden;
			for(unsigned j = 0; j < n; ++j)
			{
				p(j) = bk * p(j) + z(j);
				pp(j) = bk * pp(j) + zz(j);
			}
		}

		bkden = bknum;
		z = atimes(&p, false);

		akden = 0.0;
		for(unsigned j = 0; j < n; ++j)
			akden += z(j) * pp(j);

		ak = bknum / akden;
		zz = atimes(&pp, true);

		for(unsigned j = 0; j < n; ++j)
		{
			(*x)(j) += ak * p(j);
			r(j) -= ak * z(j);
			rr(j) -= ak * zz(j);
		}

		z = asolve(&r);

		err = snorm(&r) / bnrm;

		// std::cout << "iter: " << iter << " == x: " << *x << std::endl;

		if(checkStopCrit(err))
			break;
	}

	// std::cout << "Sparse Conjugate Gradient solved in " << iter << " iterations." << std::endl;
	// std::cout << "Solution" << (*x) << std::endl;

	return x;
}

// Private members
// -----------------------------------------------------------

double SparseConjugateGradient::snorm(Vector *v)
{
	double res = 0;
	for(unsigned i = 0; i < v->length; ++i)
		res += (*v)(i) * (*v)(i);

	res = sqrt(res);

	return res;
}

Vector SparseConjugateGradient::atimes(Vector *in, bool isTraspose)
{
	if(isTraspose)
		return A->transpose() * (*in);

	return (*A) * (*in);
}

Vector SparseConjugateGradient::asolve(Vector *b)
{
	Vector x(b->length);
	for(unsigned int i = 0; i < A->getRowCount(); ++i)
	{
		double diagonal = (*A)(i, i);
		x(i) = diagonal != 0 ? (*b)(i) / diagonal : (*b)(i);
	}

	return x;
}

bool SparseConjugateGradient::checkStopCrit(double err)
{
	if(err <= tol && -tol <= err)
		return true;

	return false;
}
