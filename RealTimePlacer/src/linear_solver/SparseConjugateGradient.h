/*
 * SparseConjugateGradient.h
 *
 *  Created on: Jan 10, 2013
 *      Author: Matara Ma Sukoy
 */

#ifndef SPARSECONJUGATEGRADIENT_H_
#define SPARSECONJUGATEGRADIENT_H_

#include "../data_structure/SparseMatrix.h"
#include "../data_structure/Vector.h"

class SparseConjugateGradient
{
public:
	SparseConjugateGradient();
	SparseConjugateGradient(SparseMatrix* A, Vector* B);
	virtual ~SparseConjugateGradient();

	Vector* solve(bool isTransposed = false);

	SparseMatrix* A;
	Vector* x;
	Vector* B;
	int iterMax;
	double tol; // Convergence Tolerance

private:
	double snorm(Vector *v);
	Vector atimes(Vector *in, bool isTrasposed);
	Vector asolve(Vector *b);

	bool checkStopCrit(double err);
};

#endif /* SPARSECONJUGATEGRADIENT_H_ */
