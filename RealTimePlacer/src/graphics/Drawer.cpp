/*
 * Drawer.cpp
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>

#include "../MainConfiguration.h"

#include "Drawer.h"

#if ENABLE_DRAWING_INTERSECTION_LINE

inline void findIntersectionPoints(Node* n1, Node* n2, Position& p1, Position& p2);
inline bool hitTestNode(Node* source, Node* destination);
inline bool hitTest(Position& p1TopLeft, Position& p1BottomRight, Position& p2TopLeft, Position& p2BottomRight);

#endif

Drawer::Drawer(Board* board, Graph* graph,
		double drawBoardTopLeftX, double drawBoardTopLeftY,
		double drawBoardBottomRightX, double drawBoardBottomRightY,
		bool areBordersDrawn, bool isROIDrawn)
: board(board),
  graph(graph),
  grid(board->grid),
  drawBoardTopLeftX(drawBoardTopLeftX),
  drawBoardTopLeftY(drawBoardTopLeftY),
  drawBoardBottomRightX(drawBoardBottomRightX),
  drawBoardBottomRightY(drawBoardBottomRightY),
  drawBoardHorizontalLength(drawBoardTopLeftX - drawBoardBottomRightX),
  drawBoardVerticalLength(drawBoardTopLeftY - drawBoardBottomRightY),
  drawBoardCellLengthX(0),
  drawBoardCellLengthY(0),
  selectedNodeId(-1),
  selectedGridCellIdi(-1),
  selectedGridCellIdj(-1),
  selectedNetId(-1),
  areBordersDrawn(areBordersDrawn),
  isROIDrawn(isROIDrawn)
{
	drawBoardCellLengthX = (grid->cellLengthX / board->dimX) * drawBoardHorizontalLength;
	drawBoardCellLengthY = (grid->cellLengthY / board->dimY) * drawBoardVerticalLength;
}

Drawer::~Drawer(void)
{
}

// Members
// -----------------------------------------------------------------------

void Drawer::drawAll(void)
{
	drawGrid();
	drawNodes();

	if(isROIDrawn)
		drawROI();

	if(areBordersDrawn)
		drawBorders();

	drawSelectedNet();
#if ENABLE_DRAWING_INTERSECTION_LINE
	drawIntersectionDiagonals();
#endif
}

void Drawer::drawROI(void)
{
	glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 0.0f); // BLACK
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* currNode = graph->getNode(i);
#if ROI_SHAPE_USED == ROI_CIRCLE
		drawCircle(currNode->pos, currNode->roiRadius);
#else // if ROI_SHAPE_USED == ROI_RECTANGLE
		Position topLeft = currNode->calcROITopLeftPos();
		Position bottomRight = currNode->calcROIBottomRightPos();
		drawRectangle(topLeft, bottomRight);
#endif
	}
}

void Drawer::drawNodes(void)
{
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* currNode = graph->getNode(i);

		if(currNode->isTerminal())
			glColor3f(1.0f, 0.0f, 0.0f); // RED
		else if(currNode->isSudo()) // TODO: in current implementation sudo nodes are not drawn since they don't have any dimX/dimY
			glColor3f(0.0f, 1.0f, 0.0f); // GREEN
		else // normal
			glColor3f(0.0f, 0.0f, 1.0f); // BLUE

		drawNode(*currNode);
	}

	// Redraw selected node to appear on top & Highlight
	if(selectedNodeId >= 0)
	{
		glColor3f(0.8f, 0.8f, 0.8f); // GRAY
		Node* selectedNode = graph->getNode(selectedNodeId);

		drawNode(*selectedNode);

		// also draw this nodes relations
		glLineWidth(3.0f);
		glColor3f(0.0f, 1.0f, 0.0f); // GREEN
		drawNodeRelations(selectedNode);
		glLineWidth(1.0f);
	}
}

void Drawer::drawSelectedNet(void)
{
	if(selectedNetId >= 0)
	{
		glLineWidth(3.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glBegin(GL_LINES);
		{
			Net* selectedNet = graph->getNet(selectedNetId);
			drawNet(selectedNet);
		}
		glEnd();
		glLineWidth(1.0f);
	}
}

void Drawer::drawGrid(void)
{
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	{
		glColor3f(0.0, 0.0, 0.0);

		// draw horizontal lines
		for(int i = 0; i <= board->grid->rows; ++i)
		{
			glVertex3f(drawBoardTopLeftX, drawBoardTopLeftY - i * drawBoardCellLengthY, 0.0f);
			glVertex3f(drawBoardBottomRightX, drawBoardTopLeftY - i * drawBoardCellLengthY, 0.0f);
		}

		// draw vertical lines
		for(int i = 0; i <= board->grid->cols; ++i)
		{
			glVertex3f(drawBoardTopLeftX - i * drawBoardCellLengthX, drawBoardTopLeftY, 0.0f);
			glVertex3f(drawBoardTopLeftX - i * drawBoardCellLengthX, drawBoardBottomRightY, 0.0f);
		}
	}
	glEnd();

	// Highlight selected grid cell
	if(selectedGridCellIdi >= 0 && selectedGridCellIdj >= 0)
	{
		glBegin(GL_QUADS);
		{
			// calculate corner coordinates
			Position topLeft(grid->cellLengthX * selectedGridCellIdj,
					grid->cellLengthY * selectedGridCellIdi);
			Position bottomRight(grid->cellLengthX * (selectedGridCellIdj + 1),
					grid->cellLengthY * (selectedGridCellIdi + 1));

			convertFromBoardToGL(topLeft);
			convertFromBoardToGL(bottomRight);

			glColor3f(0.0f, 0.6f, 0.6f);
			glVertex3f(topLeft.x, topLeft.y, 0.0f);
			glVertex3f(bottomRight.x, topLeft.y, 0.0f);
			glVertex3f(bottomRight.x, bottomRight.y, 0.0f);
			glVertex3f(topLeft.x, bottomRight.y, 0.0f);
		}
		glEnd();
	}
}

void Drawer::drawBorders(void)
{
	glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* node = graph->getNode(i);

		Position topLeft = node->calcTopLeftPos();
		Position bottomRight = node->calcBottomRightPos();
		drawRectangle(topLeft, bottomRight);
	}
}

// Methods to handle GUI selections
// ------------------------------------------------------------------------

void Drawer::setSelectedGridCellId(int selectedGridCellNo)
{
	selectedGridCellIdi = selectedGridCellNo / board->grid->cols;
	selectedGridCellIdj = selectedGridCellNo % board->grid->cols;
}

void Drawer::setSelectedNodeId(int selectedNodeId)
{
	this->selectedNodeId = selectedNodeId;
}

void Drawer::setSelectedNetId(int selectedNetId)
{
	this->selectedNetId = selectedNetId;
}

void Drawer::toogleROIDrawing(void)
{
	isROIDrawn = !isROIDrawn;
}

void Drawer::toogleBorderDrawing(void)
{
	areBordersDrawn = !areBordersDrawn;
}

// Private Members
// ----------------------------------------------------------------------

// Some primitives to help drawing
// ----------------------------------------------------------------------

void Drawer::drawNodeRelations(Node* node)
{
	for(unsigned int i = 0; i < node->nets.size(); ++i)
	{
		unsigned int netId = node->getNetId(i);
		Net* net = graph->getNet(netId);

		for(unsigned int j = 0; j < net->nodes.size(); ++j)
		{
			int neighborId = net->getNodeId(j);
			Node* neighbor = graph->getNode(neighborId);

			Position p1 = node->pos;
			Position p2 = neighbor->pos;

			convertFromBoardToGL(p1);
			convertFromBoardToGL(p2);

			glBegin(GL_LINES);
			{
				glVertex3f(p1.x, p1.y, 0.0);
				glVertex3f(p2.x, p2.y, 0.0);
			}
			glEnd();
		}
	}
}

void Drawer::drawNet(Net* net)
{
	glLineWidth(3.0f);

	for(unsigned int i = 0; i < net->nodes.size(); ++i)
	{
		int n1Id = net->nodes[i];
		Node* n1 = graph->getNode(n1Id);

		Position topLeft = n1->calcTopLeftPos();
		Position bottomRight = n1->calcBottomRightPos();

		drawRectangle(topLeft, bottomRight);
	}

	glLineWidth(1.0f);
}

void Drawer::drawNode(const Node& node)
{
	Position borderCoors[4];
	findBorderRelativeCoors(node, borderCoors);

	glBegin(GL_QUADS);
	{
		glVertex3f(borderCoors[0].x, borderCoors[0].y, 0.0f);
		glVertex3f(borderCoors[2].x, borderCoors[2].y, 0.0f);
		glVertex3f(borderCoors[3].x, borderCoors[3].y, 0.0f);
		glVertex3f(borderCoors[1].x, borderCoors[1].y, 0.0f);
	}
	glEnd();
}

void Drawer::drawRectangle(Position& topLeft, Position& bottomRight)
{
	convertFromBoardToGL(topLeft);
	convertFromBoardToGL(bottomRight);

	glBegin(GL_LINES);
	{
		glVertex3f(topLeft.x, topLeft.y , 0.0f);
		glVertex3f(bottomRight.x, topLeft.y , 0.0f);
		glVertex3f(bottomRight.x, topLeft.y , 0.0f);
		glVertex3f(bottomRight.x, bottomRight.y , 0.0f);
		glVertex3f(bottomRight.x, bottomRight.y , 0.0f);
		glVertex3f(topLeft.x, bottomRight.y , 0.0f);
		glVertex3f(topLeft.x, bottomRight.y , 0.0f);
		glVertex3f(topLeft.x, topLeft.y , 0.0f);
	}
	glEnd();
}

void Drawer::drawCircle(Position centerPos, double& radius)
{
	Position temp = centerPos;
	temp.x += radius;
	convertFromBoardToGL(centerPos);
	convertFromBoardToGL(temp);

	double glRadius = sqrt(pow(centerPos.x - temp.x, 2) + pow(centerPos.y - temp.y, 2));

	glBegin(GL_LINE_LOOP);
	int numSegments = 18;
	for(int ii = 0; ii < numSegments; ii++)
	{
		float theta = 2.0f * 3.1415926f * float(ii) / float(numSegments);//get the current angle

		float x = glRadius * cosf(theta);//calculate the x component
		float y = glRadius * sinf(theta);//calculate the y component

		glVertex3f(x + centerPos.x, y + centerPos.y, 0.0f);//output vertex

	}
	glEnd();
}
#if ENABLE_DRAWING_INTERSECTION_LINE
void Drawer::drawIntersectionDiagonals(void)
{
	for(unsigned i = 0; i < graph->getTotalNodeCount(); ++i)
	{
		Node* n1 = graph->getNode(i);
		for(unsigned j = 0; j < graph->getTotalNodeCount(); ++j)
		{
			if(i == j)
				continue;

			Node* n2 = graph->getNode(j);

			if(!hitTestNode(n1, n2))
				continue;

			Position p1;
			Position p2;
			findIntersectionPoints(n1, n2, p1, p2);

			convertFromBoardToGL(p1);
			convertFromBoardToGL(p2);

			glColor3f(0.0f, 0.0f, 0.0f);
			glLineWidth(3.0f);
			glBegin(GL_LINES);
			{
				glVertex3f(p1.x, p1.y, 0.0f);
				glVertex3f(p2.x, p2.y, 0.0f);
			}
			glEnd();
			glLineWidth(1.0f);
		}
	}
}
#endif
// Other private members for mostly converting coordinates between board and GL
// ----------------------------------------------------------------------

void Drawer::findBorderRelativeCoors(const Node &node, Position* borderCoors)
{
	borderCoors[0] = Position(node.pos.x - node.dimX / 2, node.pos.y - node.dimY / 2); // top left
	borderCoors[1] = Position(node.pos.x + node.dimX / 2, node.pos.y - node.dimY / 2); // top right
	borderCoors[2] = Position(node.pos.x - node.dimX / 2, node.pos.y + node.dimY / 2); // bottom left
	borderCoors[3] = Position(node.pos.x + node.dimX / 2, node.pos.y + node.dimY / 2); // bottom right

	convertFromBoardToGL(borderCoors[0]);
	convertFromBoardToGL(borderCoors[1]);
	convertFromBoardToGL(borderCoors[2]);
	convertFromBoardToGL(borderCoors[3]);
}

void Drawer::convertFromBoardToGL(Position& original)
{
	// std::cout << "original x: " << original.x << " original.y: " << original.y << std::endl;
	original.x = convertXFromBoardToGL(original.x);
	original.y = convertYFromBoardToGL(original.y);
	// std::cout << "converted x: " << original.x << " converted.y: " << original.y << std::endl;
}

double Drawer::convertXFromBoardToGL(double& f)
{
	// std::cout << "(f / board->dimX): " << (f / board->dimX) << std::endl;
	// std::cout << "board->dimX: " << board->dimX << " f: " <<  f << std::endl;
	return -((f / board->dimX) * drawBoardHorizontalLength - drawBoardHorizontalLength / 2);
}

double Drawer::convertYFromBoardToGL(double& f)
{
	return -((f / board->dimY) * drawBoardVerticalLength - drawBoardVerticalLength / 2);
}

void Drawer::convertFromGLToBoard(Position& glPos)
{
	glPos.x = convertXFromGLToBoard(glPos.x);
	glPos.y = convertYFromGLToBoard(glPos.y);
}

void Drawer::convertFromGLToBoard(double& x, double& y)
{
	x = convertXFromGLToBoard(x);
	y = convertYFromGLToBoard(y);
}

double Drawer::convertXFromGLToBoard(double &f)
{
	return ((f + drawBoardHorizontalLength / 2) / drawBoardHorizontalLength) * board->dimX;
}

double Drawer::convertYFromGLToBoard(double &f)
{
	return ((f + drawBoardVerticalLength / 2) / drawBoardVerticalLength) * board->dimY;
}

// TODO below this line is just for debug
// ---------------------------------------------
#if ENABLE_DRAWING_INTERSECTION_LINE
inline void findIntersectionPoints(Node* n1, Node* n2, Position& p1, Position& p2)
{
	Position n1TopLeft = n1->calcTopLeftPos();
	Position n1BottomRight = n1->calcBottomRightPos();
	Position n2TopLeft = n2->calcTopLeftPos();
	Position n2BottomRight = n2->calcBottomRightPos();

	if(n1TopLeft.x > n2TopLeft.x)
	{
		if(n1BottomRight.x > n2BottomRight.x)
		{
			p1.x = n1TopLeft.x;
			p2.x = n2BottomRight.x;
		}
		else
		{
			p1.x = n1TopLeft.x;
			p2.x = n1BottomRight.x;
		}
	}
	else // if(n2TopLeft.x > n1TopLeft.x)
	{
		if(n2BottomRight.x > n1BottomRight.x)
		{
			p1.x = n2TopLeft.x;
			p2.x = n1BottomRight.x;
		}
		else
		{
			p1.x = n2TopLeft.x;
			p2.x = n2BottomRight.x;
		}
	}

	if(n1TopLeft.y > n2TopLeft.y)
	{
		if(n1BottomRight.y > n2BottomRight.y)
		{
			p1.y = n2BottomRight.y;
			p2.y = n1TopLeft.y;
		}
		else
		{
			p1.y = n1TopLeft.y;
			p2.y = n1BottomRight.y;
		}
	}
	else // if(n2TopLeft.y > n1TopLeft.y)
	{
		if(n2BottomRight.y > n1BottomRight.y)
		{
			p1.y = n2TopLeft.y;
			p2.y = n1BottomRight.y;
		}
		else
		{
			p1.y = n2TopLeft.y;
			p2.y = n2BottomRight.y;
		}
	}
}

inline bool hitTestNode(Node* source, Node* destination)
{
	Position sourceTopLeft = source->calcTopLeftPos();
	Position sourceBottomRight = source->calcBottomRightPos();
	Position destinationTopLeft = destination->calcTopLeftPos();
	Position destinationBottomRight = destination->calcBottomRightPos();

	return hitTest(sourceTopLeft, sourceBottomRight, destinationTopLeft, destinationBottomRight);
}

inline bool hitTest(Position& p1TopLeft, Position& p1BottomRight, Position& p2TopLeft, Position& p2BottomRight)
{
	if(p1BottomRight.x >= p2TopLeft.x &&
		p1TopLeft.x <= p2BottomRight.x &&
		p1BottomRight.y >= p2TopLeft.y &&
		p1TopLeft.y <= p2BottomRight.y)
	{
		return true;
	}

	return false;
}
#endif
