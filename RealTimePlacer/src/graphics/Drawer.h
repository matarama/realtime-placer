/*
 * Drawer.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef DRAWER_H_
#define DRAWER_H_

#include "../boid/Position.h"

#include "../vlsi_structure/Board.h"
#include "../vlsi_structure/Node.h"
#include "../vlsi_structure/Graph.h"

class Drawer
{
public:
	Drawer(Board* board, Graph* graph,
		double drawBoardTopLeftX = 14.0f,
		double drawBoardTopLeftY = 10.0f,
		double drawBoardBottomRightX = -14.0f,
		double drawBoardBottomRightY = -10.0f,
		bool areNetsDrawn = false, bool isROIDrawn = false);

	virtual ~Drawer(void);

	void drawAll(void);
	void drawNodes(void);
	void drawGrid(void);
	void drawROI(void);
	void drawSelectedNet(void);
	void drawBorders(void);

	// TODO just for debug, might be removed in the future
	void drawIntersectionDiagonals(void);

	void setSelectedNodeId(int selectedNodeId);
	void setSelectedNetId(int selectedNetId);
	void setSelectedGridCellId(int selectedGridCellNo);

	void toogleROIDrawing(void);
	void toogleBorderDrawing(void);

	Board* board;
	Graph* graph;
	Grid* grid;
	double drawBoardTopLeftX;
	double drawBoardTopLeftY;
	double drawBoardBottomRightX;
	double drawBoardBottomRightY;
	double drawBoardHorizontalLength;
	double drawBoardVerticalLength;
	double drawBoardCellLengthX;
	double drawBoardCellLengthY;

private:
	void drawNodeRelations(Node* node);
	void drawNet(Net* net);
	void drawNode(const Node& node);
	void drawCircle(Position centerPos, double& radius);
	void drawRectangle(Position& topLeft, Position& bottomRight);

	void findBorderRelativeCoors(const Node& node, Position* borderCoors);

	void convertFromBoardToGL(Position& original);
	double convertXFromBoardToGL(double& f);
	double convertYFromBoardToGL(double& f);

	void convertFromGLToBoard(Position& glPos);
	void convertFromGLToBoard(double& x, double& y);
	double convertXFromGLToBoard(double &f);
	double convertYFromGLToBoard(double &f);

	int selectedNodeId;
	int selectedGridCellIdi;
	int selectedGridCellIdj;
	int selectedNetId;

	bool areBordersDrawn;
	bool isROIDrawn;
};

#endif /* DRAWER_H_ */
