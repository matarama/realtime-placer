/*
 * BasicGLPane.h
 *
 *  Created on: Jun 17, 2013
 *      Author: matara
 */

#ifndef BASICGLPANE_H_
#define BASICGLPANE_H_

#include <string>

#include <wx/wx.h>
#include <wx/glcanvas.h>

#include "../vlsi_structure/Graph.h"
#include "../vlsi_structure/Board.h"

#include "../io/Stats.h"

#include "../boid/Boid.h"

#include "Drawer.h"

#include "../runtime/WireLengthCalculator.h"
#include "../runtime/CongestionAnalyzer.h"

class BasicGLPane : public wxGLCanvas
{
public:
	BasicGLPane(wxWindow* parent, int* args = 0);
	virtual ~BasicGLPane();

	void render(void);
	void render(wxPaintEvent& evt);
	void redraw(void);

	void setSelectedNodeId(int selection);
	void setSelectedNetId(int selection);
	void setSelectedGridCellId(int selection);
	void toogleRelationDrawing(void);
	void toogleROIDrawing(void);
	void simulate(void);
	void simulateStep(void);

	std::string calcDiagonalWL(void);
	std::string calcHalfPerimeterWL(void);
	std::string getCongestionRatio(void);
	std::string getCongestionArea(void);
	std::string getBoardArea(void);
	std::string getTotalCellArea(void);
	std::string getPlacementDensity(void);

	// events
	void mouseMoved(wxMouseEvent& event);
	void mouseDown(wxMouseEvent& event);
	void mouseWheelMoved(wxMouseEvent& event);
	void mouseReleased(wxMouseEvent& event);
	void rightClick(wxMouseEvent& event);
	void mouseLeftWindow(wxMouseEvent& event);
	void keyPressed(wxKeyEvent& event);
	void keyReleased(wxKeyEvent& event);

	Graph* getGraph(void) { return graph; }
	Stats* getStats(void) { return stats; }
	Board* getBoard(void) { return board; }
	Drawer* getDrawer(void) { return drawer; }

	DECLARE_EVENT_TABLE()
private:
	void initTool(void);
	void prepareViewport(void);

	wxGLContext* m_context;

	Graph* graph;
	Stats* stats;
	Board* board;
	Drawer* drawer;
	Boid* boid;
	WireLengthCalculator* wlCalculator;
	CongestionAnalyzer* congestionAnalyzer;
};

#endif /* BASICGLPANE_H_ */
