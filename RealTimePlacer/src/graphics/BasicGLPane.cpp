
#include <iostream>
#include <sstream>
using namespace std;

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "../MainConfiguration.h"

#include "../data_structure/SparseMatrix.h"
#include "../data_structure/Vector.h"

#include "../boid/Position.h"
#include "../boid/Force.h"

#include "../quadratic_placer/QuadraticPlacer.h"

#include "../vlsi_structure/Node.h"
#include "../vlsi_structure/Net.h"

#include "../io/InputHandler.h"

#include "BasicGLPane.h"

// Event Bindings
// --------------------------------------------------------------

BEGIN_EVENT_TABLE(BasicGLPane, wxGLCanvas)
	EVT_MOTION(BasicGLPane::mouseMoved)
	EVT_LEFT_DOWN(BasicGLPane::mouseDown)
	EVT_LEFT_UP(BasicGLPane::mouseReleased)
	EVT_RIGHT_DOWN(BasicGLPane::rightClick)
	EVT_LEAVE_WINDOW(BasicGLPane::mouseLeftWindow)
	EVT_KEY_DOWN(BasicGLPane::keyPressed)
	EVT_KEY_UP(BasicGLPane::keyReleased)
	EVT_MOUSEWHEEL(BasicGLPane::mouseWheelMoved)
	EVT_PAINT(BasicGLPane::render)
END_EVENT_TABLE()


// some useful events to use
void BasicGLPane::mouseMoved(wxMouseEvent& event) {}
void BasicGLPane::mouseDown(wxMouseEvent& event)
{
	std::cout << "mouse down: " << event.m_x << " " << event.m_y << std::endl;
}
void BasicGLPane::mouseWheelMoved(wxMouseEvent& event) {}
void BasicGLPane::mouseReleased(wxMouseEvent& event) {}
void BasicGLPane::rightClick(wxMouseEvent& event) {}
void BasicGLPane::mouseLeftWindow(wxMouseEvent& event) {}
void BasicGLPane::keyPressed(wxKeyEvent& event) {}
void BasicGLPane::keyReleased(wxKeyEvent& event) {}


// Constructor
// --------------------------------------------------------------

BasicGLPane::BasicGLPane(wxWindow* parent, int* args)
: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxSize(650, 620), wxFULL_REPAINT_ON_RESIZE),
  m_context(0),
  graph(0),
  stats(0),
  board(0),
  drawer(0),
  boid(0),
  wlCalculator(0),
  congestionAnalyzer(0)
{
	m_context = new wxGLContext(this);

    // To avoid flashing on MSW
    SetBackgroundStyle(wxBG_STYLE_CUSTOM);

    initTool();
    prepareViewport();
}

BasicGLPane::~BasicGLPane()
{
	delete m_context;
	delete boid;
	delete drawer;
	delete board;
	delete stats;
	delete graph;
	delete wlCalculator;
	delete congestionAnalyzer;
}

// Members
// --------------------------------------------------------------------------

void BasicGLPane::render(void)
{
	if(!IsShown())
		return;

	wxGLCanvas::SetCurrent(*m_context);
	wxPaintDC(this); // only to be used in paint events. use wxClientDC to paint outside the paint event

	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	prepareViewport();

	//clear background screen to white
	glClearColor( 1.0f, 1.0f, 1.0f, 0.0f );

	// reset view matrix
	glLoadIdentity();

	// move view back a bit
	glTranslatef(0, 0, -40);

	drawer->drawAll();

	glFlush();
    SwapBuffers();
}

void BasicGLPane::redraw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glLoadIdentity();
	drawer->drawAll();
	glFlush();
	SwapBuffers();
}


void BasicGLPane::render(wxPaintEvent& evt)
{
	render();
}

void BasicGLPane::setSelectedNodeId(int selection)
{
	drawer->setSelectedNodeId(selection);
	render();
}

void BasicGLPane::setSelectedGridCellId(int selection)
{
	drawer->setSelectedGridCellId(selection);
	render();
}

void BasicGLPane::setSelectedNetId(int selection)
{
	drawer->setSelectedNetId(selection);
	render();
}

void BasicGLPane::toogleRelationDrawing(void)
{
	drawer->toogleBorderDrawing();
	render();
}

void BasicGLPane::toogleROIDrawing(void)
{
	drawer->toogleROIDrawing();
	render();
}

void BasicGLPane::simulate(void)
{
	static int iterCount = 0;

	while(true)
	{
		++iterCount;
		boid->simulate();
		render();

		if(boid->isStable())
			break;
	}

	congestionAnalyzer->analyze();

	std::cout << "BOID halted after " << iterCount << " iterations." << std::endl;
	graph->printShortInfo();
	std::cout << std::endl;

	// boid->printNodeLocations();
}

void BasicGLPane::simulateStep(void)
{
	boid->simulate();
	render();

	congestionAnalyzer->analyze();
}

std::string BasicGLPane::calcDiagonalWL(void)
{
	float diagonalWL = wlCalculator->calculateDiagonalWL() * 100;
	diagonalWL = round(diagonalWL) / 100.0;

	std::stringstream converter;
	converter << diagonalWL;
	return converter.str();
}

std::string BasicGLPane::calcHalfPerimeterWL(void)
{
	float halfPerimeterWL = wlCalculator->calculateHalfPerimeterWL() * 100;
	halfPerimeterWL = round(halfPerimeterWL) / 100.0;

	std::stringstream converter;
	converter << halfPerimeterWL;
	return converter.str();
}

std::string BasicGLPane::getCongestionArea(void)
{
	double congestionArea = congestionAnalyzer->getCongestedArea() * 100;
	congestionArea = round(congestionArea) / 100.0;

	std::stringstream converter;
	converter << congestionArea;
	return converter.str();
}

std::string BasicGLPane::getCongestionRatio(void)
{
	double congestionRatio = congestionAnalyzer->getCongestionRatio() * 10000;
	congestionRatio = round(congestionRatio) / 100.0;

	std::stringstream converter;
	converter << "%" << congestionRatio;
	return converter.str();
}

std::string BasicGLPane::getBoardArea(void)
{
	double boardArea = board->dimX * board->dimY;

	std::stringstream converter;
	converter << boardArea;
	return converter.str();
}

std::string BasicGLPane::getTotalCellArea(void)
{
	double totalCellArea = graph->getTotalCellArea();

	std::stringstream converter;
	converter << totalCellArea;
	return converter.str();
}

std::string BasicGLPane::getPlacementDensity(void)
{
	double density = graph->getTotalCellArea() / (board->dimX * board->dimY);
	density = round(density * 100) / 100;

	std::stringstream converter;
	converter << density;
	return converter.str();
}

// Private members
// ---------------------------------------------------------------------

void BasicGLPane::initTool(void)
{
	//InputHandler ih("benchmark_congestion_simple/test");
	//InputHandler ih("benchmark_alignment_medium/test");
	//InputHandler ih("benchmark_alignment_simple/test");
	//InputHandler ih("benchmark_intersection_test/test");
	//InputHandler ih("benchmark_boundary_simple/test");
	//InputHandler ih("benchmark_separation_complex/test");
	//InputHandler ih("benchmark_separation_medium/test");
	//InputHandler ih("benchmark_separation_simple_2/test");
	//InputHandler ih("benchmark_separation_simple/test");
	//InputHandler ih("benchmark_cohesion_multinode/test");
	//InputHandler ih("benchmark_cohesion_singlenode/test");
	//InputHandler ih("benchmark_trivial/test");
	//InputHandler ih("benchmark_base/test");
	//InputHandler ih("benchmark_homogenous/test");
	InputHandler ih("benchmark_homogenous_crowded/test");
	ih.init();

	graph = ih.graph;
	stats = ih.stats;
	cout << *graph << endl;

	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 30);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 20, 20);
	//board = new Board(graph, 40, 20);
	//board = new Board(graph, 20, 16);
	board = new Board(graph, 16, 12);

#if ENABLE_QUADRATIC_PLACEMENT

	QuadraticPlacer qp(graph);
	qp.place();

	cout << "After quadratic placement..." << endl;
	cout << *graph << endl;

#endif

	boid = new Boid(board);
	boid->printStatistics();

	drawer = new Drawer(board, graph);

	wlCalculator = new WireLengthCalculator(graph);
	congestionAnalyzer = new CongestionAnalyzer(graph);
	congestionAnalyzer->analyze();

	cout << "Stats..." << endl;
	cout << *stats << endl;
}

// -------------------------------------------------------------------------------------------------------

/** Inits the OpenGL viewport for drawing in 3D. */

void BasicGLPane::prepareViewport(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the buffer
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // White Background
    glClearDepth(1.0f);	// Depth Buffer Setup
    //glEnable(GL_DEPTH_TEST); // Enables Depth Testing
    glDepthFunc(GL_LEQUAL); // The Type Of Depth Testing To Do
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glEnable(GL_COLOR_MATERIAL);

    glViewport(0, 0, 600, 600);
    // glViewport(0, 0, 800, 600);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    float aspectRatio = (float)(600)/(float)(600);
    // float aspectRatio = (float)(600)/(float)(800);
    glFrustum(.5, -.5, -.5 * aspectRatio, .5 * aspectRatio, 1, 50);
    glMatrixMode(GL_MODELVIEW);

}
