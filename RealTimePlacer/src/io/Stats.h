/*
 * Stats.h
 *
 *  Created on: Jun 8, 2013
 *      Author: matara
 */

#ifndef STATS_H_
#define STATS_H_

#include <string>
#include <ostream>

struct Stats
{
	friend std::ostream& operator<<(std::ostream& out, Stats& stats)
	{
		out << "----------------------------------------------------" << std::endl;
		out << "Printing stats..." << std::endl;
		out << "Benchmark: " << stats.benchmarkName << std::endl;
		out << "Total Node Count: " << stats.totalNodeCount << std::endl;
		out << "Terminal Count: " << stats.terminalCount << std::endl;
		out << "Clique-nets with size = 2: " << stats.netCountClique2 << std::endl;
		out << "Clique-nets with size = 3: " << stats.netCountClique3 << std::endl;
		out << "Clique-nets with size > 3: " << stats.netCountCliqueMore << std::endl;
		out << "Total Clique nets: " << stats.netCountClique2 + stats.netCountClique3 + stats.netCountCliqueMore << std::endl;
		out << "Star nets: " << stats.netCountStar << std::endl;
		out << "----------------------------------------------------" << std::endl;

		return out;
	}

	Stats(std::string benchmarkName)
	: benchmarkName(std::string(benchmarkName)),
	  totalNetCount(0),
	  totalNodeCount(0),
	  terminalCount(0),
	  netCountClique2(0),
	  netCountClique3(0),
	  netCountCliqueMore(0),
	  netCountStar(0)
	{
	}

	std::string benchmarkName;

	unsigned int totalNetCount;
	unsigned int totalNodeCount;
	unsigned int terminalCount;
	unsigned int netCountClique2;
	unsigned int netCountClique3;
	unsigned int netCountCliqueMore;
	unsigned int netCountStar;
};


#endif /* STATS_H_ */
