/*
 * InputHandler.cpp
 *
 *  Created on: Jun 7, 2013
 *      Author: matara
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
using namespace std;

#include <stdio.h>
#include <unistd.h>
#include <climits>

#include "../MainConfiguration.h"

#include "InputHandler.h"

inline void applyCliqueNetModel(fstream& fileStream, Graph* graph, Net* net, Stats* stats, int& nodeCount);
inline void applyStarNetModel(fstream& fileStream, Graph* graph, Net* net, Stats* stats, int& nodeCount);


InputHandler::InputHandler(string benchmarkName)
: graph(0),
  stats(0)
{
	graph = new Graph();
	stats = new Stats(benchmarkName);
}

InputHandler::~InputHandler(void)
{
}

// Members
// ---------------------------------------------------------

void InputHandler::init(void)
{
	// string workingDir = get_current_dir_name();
	// cout << workingDir << endl;
	// string nodeFileName = workingDir + "/" + stats->benchmarkName + ".nodes ...";

	string nodeFileName = stats->benchmarkName + ".nodes";
	readNodes(nodeFileName);

	// cout << "Graph so far.." << endl << graph << endl;

	string netFileName = stats->benchmarkName + ".nets";
	readNets(netFileName);

	// cout << "Graph so far.." << endl << graph << endl;

	string plFileName = stats->benchmarkName + ".placement";
	readPlacement(plFileName);

	// cout << "Graph so far.." << endl << graph << endl;
}

void InputHandler::printStats(void)
{
	cout << *stats << endl;
}

// Private members
// -----------------------------------------------------

void InputHandler::readNodes(string nodeFileName)
{
	fstream nodeFileStream(nodeFileName.c_str());

	if(!nodeFileStream.is_open())
	{
		cerr << "Cannot find " << nodeFileName << "!!" << endl;
		throw;
	}

	string line;
	int nodeId;
	int nodeDimX;
	int nodeDimY;
	int nodeType;

	getline(nodeFileStream, line);
	getline(nodeFileStream, line);
	getline(nodeFileStream, line);
	getline(nodeFileStream, line);
	getline(nodeFileStream, line);
	stats->totalNodeCount = atoi(line.substr(line.find("\t")).c_str());
	getline(nodeFileStream, line);
	stats->terminalCount = atoi(line.substr(line.find("\t")).c_str());

	while(getline(nodeFileStream, line))
	{
		int tabPos = line.find("\t");
		nodeId = atoi(line.substr(1, tabPos).c_str());

		line = line.substr(tabPos + 1);
		tabPos = line.find("\t");
		nodeDimX = atoi(line.substr(0, tabPos).c_str());

		line = line.substr(tabPos + 1);
		tabPos = line.find("\t");
		nodeDimY = atoi(line.substr(0, tabPos).c_str());

		if(tabPos < 0)
			nodeType = NODE_TYPE_NON_TERMINAL;
		else
			nodeType = NODE_TYPE_TERMINAL;

		graph->addNode(nodeId, nodeDimX, nodeDimY, NODE_WEIGHT_DEFAULT, nodeType);
	}

	nodeFileStream.close();
}

void InputHandler::readNets(string netFileName)
{
	fstream netFileStream(netFileName.c_str());

	if(!netFileStream.is_open())
	{
		cerr << "Cannot find " << netFileName << "!!" << endl;
		throw;
	}

	string line;

	getline(netFileStream, line);
	getline(netFileStream, line);
	getline(netFileStream, line);
	getline(netFileStream, line);
	getline(netFileStream, line);
	stats->totalNetCount = atoi(line.substr(line.find(":") + 1).c_str());
	getline(netFileStream, line);
	getline(netFileStream, line);

	while(getline(netFileStream, line))
	{
		string netInfo = line.substr(line.find(":") + 2);
		int nodeCount = atoi(netInfo.substr(0, netInfo.find(" ")).c_str());
		int netId = atoi(netInfo.substr(netInfo.find(" ") + 2).c_str());

		Net* net = graph->addNet(netId);

#if STAR_NET_MODEL_ENABLED
		if(nodeCount > 3)
			applyStarNetModel(netFileStream, graph, net, stats, nodeCount);
		else
			applyCliqueNetModel(netFileStream, graph, net, stats, nodeCount);
#else
		applyCliqueNetModel(netFileStream, graph, net, stats, nodeCount);
#endif

	}

	netFileStream.close();
}

void InputHandler::readPlacement(string plFileName)
{
	// cout << "Reading " << plFileName << " ..." << endl;


	// Position topLeft(INT_MAX, INT_MAX);
	// Position bottomRight(0.0, 0.0);

	fstream plFileStream(plFileName.c_str());

	if(!plFileStream.is_open())
	{
		cerr << "Cannot find " << plFileName << "!!" << endl;
		throw;
	}

	string line;
	getline(plFileStream, line);
	getline(plFileStream, line);
	getline(plFileStream, line);
	getline(plFileStream, line);

	int nodeId;
	double x;
	double y;

	while(getline(plFileStream, line))
	{
		nodeId = atoi(line.substr(1, line.find("\t")).c_str());

		line = line.substr(line.find("\t") + 1);
		x = atof(line.substr(0, line.find("\t")).c_str());

		line = line.substr(line.find("\t") + 1);
		y = atof(line.substr(0, line.find("\t")).c_str());

		Node* node = graph->getNode(nodeId);
		//if(node->isTerminal())
		{
			node->pos.x = x;
			node->pos.y = y;
		}
	}

	plFileStream.close();

	// cout << "Top Left: " << topLeft << endl;
	// cout << "Bottom Right: " << bottomRight << endl;
}


// Some helper methods
// ----------------------------------------------------------------------

inline void applyCliqueNetModel(fstream& fileStream, Graph* graph, Net* net, Stats* stats, int& nodeCount)
{
	// read node-ids
	std::string line;
	int nodeIdArr[nodeCount];
	for(int i = 0; i < nodeCount; ++i)
	{
		getline(fileStream, line);
		nodeIdArr[i] = atoi(line.substr(line.find("\t") + 2, line.find(" ")).c_str());

		graph->addNodeToNet(nodeIdArr[i], net->id);
		graph->addNetToNode(net->id, nodeIdArr[i]);
	}

	net->type = NET_TYPE_CLIQUE;

	// add relations
	for(int i = 0; i < nodeCount; ++i)
		for(int j = i + 1; j < nodeCount; ++j)
			graph->addRelation(nodeIdArr[i], nodeIdArr[j], net->id);

	// for recording stats
	if(nodeCount == 2)
		++(stats->netCountClique2);
	else if(nodeCount == 3)
		++(stats->netCountClique3);
	else
		++(stats->netCountCliqueMore);
}

inline void applyStarNetModel(fstream& fileStream, Graph* graph, Net* net, Stats* stats, int& nodeCount)
{
	unsigned int sudoNodeId = graph->addSudoNode(net->id, nodeCount);

	std::string line;
	int nodeId;
	for(int i = 0; i < nodeCount; ++i)
	{
		getline(fileStream, line);
		nodeId = atoi(line.substr(line.find("\t") + 2, line.find(" ")).c_str());

		graph->addRelation(sudoNodeId, nodeId, net->id, nodeCount);

		graph->addNodeToNet(nodeId, net->id);
		graph->addNetToNode(net->id, nodeId);
	}

	net->type = NET_TYPE_STAR;

	++(stats->netCountStar);
}
