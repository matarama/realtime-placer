/*
 * InputHandler.h
 *
 *  Created on: Jun 7, 2013
 *      Author: matara
 */

#ifndef INPUTHANDLER_H_
#define INPUTHANDLER_H_

#include <string>
using namespace std;

#include "Stats.h"
#include "../vlsi_structure/Graph.h"
#include "../vlsi_structure/Board.h"

class InputHandler
{
public:
	InputHandler(string benchmarkName);
	virtual ~InputHandler(void);

	void init(void);
	void printStats(void);

	Graph* graph;
	Stats* stats;
private:
	void readNodes(string nodeFileName);
	void readNets(string netFileName);
	void readPlacement(string plFileName);
};

#endif /* INPUTHANDLER_H_ */
