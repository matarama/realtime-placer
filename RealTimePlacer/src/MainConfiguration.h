
#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

// Some boolean flags for readability
// -------------------------------------------------------------------------------
#define FALSE 0
#define TRUE 1

// Net model
// -------------------------------------------------------------------------------
// Enables star-net model, if this is not enabled clique-net model is used for all
// nets, if it is enabled
// - star-net model is used for nets that have +4 nodes
// - clique-net model is used for nets that have [2 3] nodes

#define STAR_NET_MODEL_ENABLED FALSE

// Object Representation
// -------------------------------------------------------------------------------
// Is object represented by center of gravity, this is used to determine if a node
// belongs to a bin by checking only its center of gravity. If not enabled, a node
// can be in multiple bins depending on its position (TODO: this feature is not
// implemented yet).

#define OBJ_REPRESENTED_BY_COG TRUE

// Quadratic Placer
// -------------------------------------------------------------------------------
// Disables or enables quadratic placement prior to global boid placement.

#define ENABLE_QUADRATIC_PLACEMENT TRUE

// BOID Boundary Rule
// -------------------------------------------------------------------------------
// Uses normal boundary rules in which board boundaries act as a wall,
// so that no elements can get out (cuts velocity and forces in the
// direction towards wall

#define BOUNDARY_RULE 0

// If node tries to get out of the board, instead of a wall effect, it will
// just teleport to the opposite direction on the board. For example, it it
// tries to go to left, it will teleport to right while preserving original
// velocity and forces

#define TELEPORTATION_RULE 1

// Actual flag used in code
#define BOUNDARY_RULE_USED BOUNDARY_RULE


// BOID Separation Rule
// -------------------------------------------------------------------------------
// The shape of region of interests each node.

// If it is circle than the object is represented by a circle whose area is as
// same as the nodes.

#define ROI_CIRCLE 0

// If it is rectangle than the object is represented by a rectangle with the same
// area as node.

#define ROI_RECTANGLE 1

// Actual flag used in code
#define ROI_SHAPE_USED ROI_CIRCLE

// ROI rectangle separation models used to determine separation force and its direction
#define RRSM_1 1
#define RRSM_2 2
#define RRSM_3 3
#define RRSM_4 4
#define RRSM_CIRCLE_HOMOGENOUS 5
#define RRSM_CIRCLE_AREA_SENSITIVE 6

// Actual flag used to determine the RRSM used in code.
#define ROI_RECTANGLE_SEPARATION_MODEL_USED RRSM_1


// DEBUG
// Things defined here is just for debug puposes no need to include them in actual code.
// TODO: they might be removed in the future to not mess up the code readability.
// -------------------------------------------------------------------------------
#define ENABLE_DRAWING_INTERSECTION_LINE FALSE


// Some default values that control the behavior of simulation
// -------------------------------------------------------------------------------
// BOID_FRICTION_CONSTANT
const double D = 1.0;

// BOID_AIR_DENSITY
const double p = 0.4;

// Time elapsed per move (seconds)
const double t = 0.1;

// if force is less than this value it is considered to be zero
const double MIN_FORCE = 0.1;

// if velocity is less than this value node is considered to be stable
const double MIN_VELOCITY = 0.001;

// Used to calculate ROI area for each node
const double DENSITY = 1.0; // [0 1]

// other constants
const double PI = 3.14159265359;

#endif // CONFIGURATION_H_
