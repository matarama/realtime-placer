#include <iostream>
#include <ctime>
#include <string>
#include <sstream>
#include <wx/wx.h>
#include <wx/grid.h>
#include <wx/notebook.h>
#include <wx/panel.h>
#include <wx/splitter.h>
#include <wx/window.h>
#include <wx/listbox.h>
#include <wx/statusbr.h>
#include <wx/sizer.h>
#include <wx/glcanvas.h>
#include <wx/string.h>
#include <wx/checkbox.h>
#include <wx/stattext.h>

#include "graphics/BasicGLPane.h"

#include "vlsi_structure/Graph.h"
#include "vlsi_structure/Node.h"
#include "boid/Force.h"
#include "boid/Position.h"


const std::string BOARD_AREA_LABEL = "Board Area: ";
const std::string TOTAL_CELL_AREA_LABEL = "Total Cell Area: ";
const std::string DENSITY_LABEL = "Placement Density: ";
const std::string DIAG_WL_LABEL = "Diagonal Wirelength: ";
const std::string HP_WL_LABEL = "Half-Perimeter Wirelength: ";
const std::string CONGESTION_AREA_LABEL = "Congestion Area: ";
const std::string CONGESTION_RATIO_LABEL = "Congestion Ratio: ";

class MyApp: public wxApp
{
    virtual bool OnInit();

    wxFrame* frame;
};

class MyFrame: public wxFrame
{
public:

    MyFrame(const wxString& title,
           const wxPoint& pos, const wxSize& size);

    void OnQuit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void HandleNodeListBox(wxCommandEvent& event);
    void HandleNetListBox(wxCommandEvent& event);
    void HandleGridListBox(wxCommandEvent& event);
    void HandleSimulate(wxCommandEvent& event);
    void HandleSimulateStep(wxCommandEvent& event);
    void ToogleROIDrawing(wxCommandEvent& WXUNUSED(event));
	void ToogleRelationDrawing(wxCommandEvent& WXUNUSED(event));
    void RefreshData(void);

    wxPanel* window1;
    wxPanel* window2;
    wxPanel* window3;
    wxListBox* nodeListBox;
    wxListBox* netListBox;
    wxListBox* gridListBox;
    wxButton* simulateButton;
    wxButton* simulateStepButton;
    wxCheckBox* drawRelationCb;
    wxCheckBox* drawROICb;
    BasicGLPane* basicGLPane;

    wxStaticText* boardAreaLabel;
    wxStaticText* totalCellAreaLabel;
    wxStaticText* densityLabel;
    wxStaticText* diagonalWLLabel;
    wxStaticText* halfPerimeterWLLabel;
    wxStaticText* congestionAreaLabel;
    wxStaticText* congestionRatioLabel;

    wxStaticText* metricBoardArea;
    wxStaticText* metricTotalCellArea;
    wxStaticText* metricDensity;
    wxStaticText* metricDiagonalWL;
    wxStaticText* metricHalfPerimeterWL;
    wxStaticText* metricCongestionArea;
    wxStaticText* metricCongestionRatio;

    DECLARE_EVENT_TABLE()
};

enum
{
    ID_Quit = 1,
    ID_About,
    ID_NodeListBox,
    ID_GridListBox,
    ID_NetListBox,
    ID_Simulate,
    ID_Simulate_Step,
    ID_GL_PANE,
    ID_ROI_Checkbox,
    ID_Relation_Checkbox,
    ID_Static_Text
};

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(ID_Quit, MyFrame::OnQuit)
    EVT_MENU(ID_About, MyFrame::OnAbout)
    EVT_LISTBOX(ID_NodeListBox, MyFrame::HandleNodeListBox)
    EVT_LISTBOX(ID_NetListBox, MyFrame::HandleNetListBox)
    EVT_LISTBOX(ID_GridListBox, MyFrame::HandleGridListBox)
    EVT_BUTTON(ID_Simulate, MyFrame::HandleSimulate)
    EVT_BUTTON(ID_Simulate_Step, MyFrame::HandleSimulateStep)
    EVT_CHECKBOX(ID_Relation_Checkbox, MyFrame::ToogleRelationDrawing)
    EVT_CHECKBOX(ID_ROI_Checkbox, MyFrame::ToogleROIDrawing)
END_EVENT_TABLE()

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    MyFrame *frame = new MyFrame( wxT("Real-time placer"),
         wxPoint(50,50), wxSize(1200, 700) );

    frame->ShowFullScreen(true);
    frame->Show(TRUE);
    SetTopWindow(frame);
    return TRUE;
}

MyFrame::MyFrame(const wxString& title,
       const wxPoint& pos, const wxSize& size)
: wxFrame((wxFrame *)NULL, -1, title, pos, size)
{
	wxNotebook* notebook = new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxSize(1200, 700));
	wxBoxSizer* mainSizer = new wxBoxSizer(wxVERTICAL);
	mainSizer->Add(notebook, wxEXPAND, wxEXPAND);

	window1 = new wxPanel(notebook, wxID_ANY);
	window2 = new wxPanel(notebook, wxID_ANY);
	window3 = new wxPanel(notebook, wxID_ANY);

	notebook->AddPage(window1, wxT("Tab1"), true);
	notebook->AddPage(window2, wxT("Tab2"), false);
	notebook->AddPage(window3, wxT("Tab3"), false);

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};
	basicGLPane = new BasicGLPane(window1, args);

	nodeListBox = new wxListBox(window1, ID_NodeListBox, wxPoint(660, 0), wxSize(300, 400));
	// nodeListBox->SetSelection(wxLB_SINGLE);

	gridListBox = new wxListBox(window1, ID_GridListBox, wxPoint(660, 410), wxSize(300, 320));
	// gridListBox->SetSelection(wxLB_SINGLE);

	netListBox = new wxListBox(window1, ID_NetListBox, wxPoint(970, 0), wxSize(380, 400));

	simulateStepButton = new wxButton(window1, ID_Simulate_Step, wxT("Just a step"), wxPoint(450, 680));
    simulateButton = new wxButton(window1, ID_Simulate, wxT("Simulate"), wxPoint(550, 680));

    drawROICb = new wxCheckBox(window1, ID_ROI_Checkbox, wxT("Draw ROI"), wxPoint(20, 680));
    drawRelationCb = new wxCheckBox(window1, ID_Relation_Checkbox, wxT("Draw Borders"), wxPoint(120, 680));

    boardAreaLabel = new wxStaticText(window1, ID_Static_Text, wxString(BOARD_AREA_LABEL.c_str(), wxConvUTF8), wxPoint(970, 420));
    totalCellAreaLabel = new wxStaticText(window1, ID_Static_Text, wxString(TOTAL_CELL_AREA_LABEL.c_str(), wxConvUTF8), wxPoint(970, 440));
    densityLabel = new wxStaticText(window1, ID_Static_Text, wxString(DENSITY_LABEL.c_str(), wxConvUTF8), wxPoint(970, 460));
    diagonalWLLabel = new wxStaticText(window1, ID_Static_Text, wxString(DIAG_WL_LABEL.c_str(), wxConvUTF8), wxPoint(970, 500));
	halfPerimeterWLLabel = new wxStaticText(window1, ID_Static_Text, wxString(HP_WL_LABEL.c_str(), wxConvUTF8), wxPoint(970, 520));
	congestionAreaLabel = new wxStaticText(window1, ID_Static_Text, wxString(CONGESTION_AREA_LABEL.c_str(), wxConvUTF8), wxPoint(970, 540));
	congestionRatioLabel = new wxStaticText(window1, ID_Static_Text, wxString(CONGESTION_RATIO_LABEL.c_str(), wxConvUTF8), wxPoint(970, 560));

	metricBoardArea = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 420));
	metricTotalCellArea = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 440));
	metricDensity = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 460));
    metricDiagonalWL = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 500));
    metricHalfPerimeterWL = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 520));
    metricCongestionArea = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 540));
    metricCongestionRatio = new wxStaticText(window1, ID_Static_Text, wxT(""), wxPoint(1150, 560));

    RefreshData();
}

void MyFrame::RefreshData(void)
{
	nodeListBox->SetSelection(-1);
	nodeListBox->Clear();
	netListBox->SetSelection(-1);
	netListBox->Clear();
	gridListBox->SetSelection(-1);
	gridListBox->Clear();

	// Add nodes
	wxArrayString nodeArray;
	for(unsigned i = 0; i < basicGLPane->getGraph()->getTotalNodeCount(); ++i)
	{
		Node* node = basicGLPane->getGraph()->getNode(i);
		nodeArray.Add(wxString(node->toString().c_str(), wxConvUTF8));
	}
	nodeListBox->InsertItems(nodeArray, 0);

	// Add nets
	wxArrayString netArray;
	for(unsigned i = 0; i < basicGLPane->getGraph()->getTotalNetCount(); ++i)
	{
		Net* net = basicGLPane->getGraph()->getNet(i);
		netArray.Add(wxString(net->toString().c_str(), wxConvUTF8));
	}
	netListBox->InsertItems(netArray, 0);

	// Add grid cells
	wxArrayString gridArray;
	Grid* grid = basicGLPane->getBoard()->grid;
	grid_iterate_all_cells_ij_ptr
	{
		gridArray.Add(wxString((*grid)(i, j)->toString().c_str(), wxConvUTF8));
	}
	gridListBox->InsertItems(gridArray, 0);


	metricBoardArea->SetLabel(wxString(basicGLPane->getBoardArea().c_str(), wxConvUTF8));
	metricTotalCellArea->SetLabel(wxString(basicGLPane->getTotalCellArea().c_str(), wxConvUTF8));
	metricDensity->SetLabel(wxString(basicGLPane->getPlacementDensity().c_str(), wxConvUTF8));
	metricDiagonalWL->SetLabel(wxString(basicGLPane->calcDiagonalWL().c_str(), wxConvUTF8));
	metricHalfPerimeterWL->SetLabel(wxString(basicGLPane->calcHalfPerimeterWL().c_str(), wxConvUTF8));
	metricCongestionArea->SetLabel(wxString(basicGLPane->getCongestionArea().c_str(), wxConvUTF8));
	metricCongestionRatio->SetLabel(wxString(basicGLPane->getCongestionRatio().c_str(), wxConvUTF8));
}

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    Close(TRUE);
}

void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxMessageBox(wxT("This is a wxWindows Hello world sample"),
        wxT("About Hello World"), wxOK | wxICON_INFORMATION, this);
}

void MyFrame::HandleNodeListBox(wxCommandEvent& event)
{
	int selection = event.GetSelection();
	basicGLPane->setSelectedNodeId(selection);
	basicGLPane->render();
}

void MyFrame::HandleNetListBox(wxCommandEvent& event)
{
	int selection = event.GetSelection();
	basicGLPane->setSelectedNetId(selection);
	basicGLPane->render();
}

void MyFrame::HandleGridListBox(wxCommandEvent& event)
{
	int selection = event.GetSelection();
	basicGLPane->setSelectedGridCellId(selection);
	basicGLPane->render();
}

void MyFrame::HandleSimulate(wxCommandEvent& WXUNUSED(event))
{
	basicGLPane->simulate();
	RefreshData();
}

void MyFrame::HandleSimulateStep(wxCommandEvent& WXUNUSED(event))
{
	basicGLPane->simulateStep();
	RefreshData();
}

void MyFrame::ToogleRelationDrawing(wxCommandEvent& WXUNUSED(event))
{
	basicGLPane->toogleRelationDrawing();
}

void MyFrame::ToogleROIDrawing(wxCommandEvent& WXUNUSED(event))
{
	basicGLPane->toogleROIDrawing();
}
