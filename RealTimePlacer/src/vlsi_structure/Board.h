/*
 * Board.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <iostream>
#include <ostream>

#include "Graph.h"
#include "Grid.h"

// TODO bunları bit olarak ifade et.
const int BOARD_INSIDE = 0;
const int BOARD_EAST = 1;
const int BOARD_SOUTH_EAST = 2;
const int BOARD_SOUTH = 3;
const int BOARD_SOUTH_WEST = 4;
const int BOARD_WEST = 5;
const int BOARD_NORTH_WEST = 6;
const int BOARD_NORTH = 7;
const int BOARD_NORTH_EAST = 8;

struct Board
{
	friend std::ostream& operator<<(std::ostream& out, Board& board)
	{
		out << "Board dimensions [" << board.dimX << ", " << board.dimY << "]" << std::endl;
		out << *(board.grid) << std::endl;
		return out;
	}

	Board(Graph* graph, double dimX, double dimY)
	: graph(graph),
	  dimX(dimX),
	  dimY(dimY),
	  grid(0)
	{
		// double ratio = 0.0f;
		// if(dimX > dimY)
		//	ratio = dimY / dimX;
		// else
		//	ratio = dimX / dimY;

		// TODO: somehow fix this and determine grid-cell size dynamically
		// float cellLengthX = ratio * dimX;
		// float cellLengthY = ratio * dimY;
		double cellLengthX = 4.0f;
		double cellLengthY = 4.0f;
		int cellCountX = dimX / cellLengthX;
		int cellCountY = dimY / cellLengthY;

		grid = new Grid(graph, cellCountY, cellCountX, cellLengthX, cellLengthY, dimX, dimY);
	}

	~Board(void)
	{
		delete grid;
	}

	Graph* graph;
	double dimX;
	double dimY;
	Grid* grid;
};


#endif /* BOARD_H_ */
