/*
 * GridCell.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef GRIDCELL_H_
#define GRIDCELL_H_

#include <ostream>
#include <string>
#include <sstream>

#include "Graph.h"
#include "../boid/Force.h"

const int GRIDCELL_STATE_ACTIVE = 0;
const int GRIDCELL_STATE_STABLE = 1;

struct GridCell
{
	friend std::ostream& operator<<(std::ostream& out, GridCell& gridCell)
	{
		out << "GridCell[" << gridCell.rowId << ", " << gridCell.colId << "]" << std::endl;
		out << "Density: " << gridCell.density << std::endl;
		out << "Total Occupied Area: " << gridCell.totalOccupiedArea << std::endl;
		out << "Top Left Force: " << gridCell.topLeftForce << std::endl;
		out << "Top Right Force: " << gridCell.topRightForce << std::endl;
		out << "Bottom Left Force: " << gridCell.bottomLeftForce << std::endl;
		out << "Bottom Right Force: " << gridCell.bottomRightForce << std::endl;
		out << "Nodes:";

		for(unsigned int i = 0; i < gridCell.nodes.size(); ++i)
			out << " " << gridCell.nodes[i];

		out << std::endl;

		return out;
	}

	GridCell(int rowId, int colId,
			Position topLeft, Position topRight,
			Position bottomLeft, Position bottomRight)
	: rowId(rowId),
	  colId(colId),
	  topLeftPosition(topLeft),
	  topRightPosition(topRight),
	  bottomLeftPosition(bottomLeft),
	  bottomRightPoisiton(bottomRight),
	  density(0.0),
	  totalOccupiedArea(0.0),
	  state(GRIDCELL_STATE_ACTIVE)
	{
	}

	void reset(void)
	{
		density = 0.0;
		totalOccupiedArea = 0.0;
		nodes = std::vector<int>();
	}

	inline void addNode(Node* node, double area)
	{
		nodes.push_back(node->id);
		totalOccupiedArea += area;
	}

	inline void calculateDensity(double& cellArea)
	{
		density = totalOccupiedArea / cellArea;
	}

	inline void setState(int s)
	{
		state = s;
	}

	inline bool isStable(void)
	{
		return state == GRIDCELL_STATE_STABLE;
	}

	inline bool isActive(void)
	{
		return state == GRIDCELL_STATE_ACTIVE;
	}

	std::string toString(void)
	{
		std::stringstream converter;
		converter << *this;

		return converter.str();
	}

	int rowId;
	int colId;
	Force topLeftForce;
	Force topRightForce;
	Force bottomLeftForce;
	Force bottomRightForce;
	Position topLeftPosition;
	Position topRightPosition;
	Position bottomLeftPosition;
	Position bottomRightPoisiton;
	double density;
	double totalOccupiedArea;
	int state;
	std::vector<int> nodes;
};

#endif /* GRIDCELL_H_ */
