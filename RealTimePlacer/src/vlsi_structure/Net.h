/*
 * Net.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef NET_H_
#define NET_H_

#include <vector>
#include <ostream>
#include <sstream>
#include <string>

const int NET_TYPE_CLIQUE = 0;
const int NET_TYPE_STAR = 1;

struct Net
{
	friend std::ostream& operator<<(std::ostream& out, Net& net)
	{
		out << "Net-" << net.id;

		if(net.type == NET_TYPE_CLIQUE)
			out << " clique";
		else if(net.type == NET_TYPE_STAR)
			out << " star";

		out << " (" << net.nodes.size() << ")" << " => Nodes:";
		for(unsigned int i = 0; i < net.nodes.size(); ++i)
			out << " " << net.nodes[i];

		return out;
	}

	Net(unsigned int id, int type = 0)
	: id(id),
	  type(type)
	{
	}

	inline void addNode(int nodeId)
	{
		nodes.push_back(nodeId);
	}

	inline int getNodeId(int index)
	{
		return nodes[index];
	}

	std::string toString(void)
	{
		std::stringstream converter;
		converter << *this;

		return converter.str();
	}

	unsigned int id;
	int type;
	std::vector<int> nodes;
};


#endif /* NET_H_ */
