/*
 * Node.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef NODE_H_
#define NODE_H_

#include <string>
#include <vector>
#include <ostream>
#include <sstream>

#include "../MainConfiguration.h"

#include "../boid/Position.h"
#include "../boid/Force.h"

const int NODE_TYPE_NON_TERMINAL = 0;
const int NODE_TYPE_TERMINAL = 1;
const int NODE_TYPE_SUDO = 2;

const int NODE_WEIGHT_DEFAULT = 1;

const int NODE_STATE_STABLE = 0;				// 00000000
const int NODE_STATE_START = 1;					// 00000001
const int NODE_STATE_MOVING = 2;				// 00000010
const int NODE_STATE_ACCELERATING = 4;			// 00000100
const int NODE_STATE_DEACCELERATING = 8;		// 00001000
const int NODE_STATE_PARTIAL_ACCELERATING = 16;	// 00010000

struct Node
{
	friend std::ostream& operator<<(std::ostream& out, Node& node)
	{
		out << "Node ID: " << node.id;

		if(node.isTerminal())
			out << "-Terminal";
		else if(node.isNonTerminal())
			out << "-Normal";
		else
			out << "-Sudo";

		out << " Area: " << node.area << " Weight: " << node.weight << std::endl;
		out << " Position: " << node.pos << std::endl;
		out << " Force: " << node.force << std::endl;
		out << " Resistance: " << node.resistance << std::endl;
		out << " Velocity: " << node.velocity << std::endl;
		out << " Permanent Velocity: " << node.permanentVelocity << std::endl;

		out << " Nets:";
		for(unsigned int i = 0; i < node.nets.size(); ++i)
			out << " " << node.nets[i];

		return out;
	}

	Node(unsigned int id, double dimX, double dimY, int weight, int nodeType = NODE_TYPE_NON_TERMINAL)
	: id(id),
	  dimX(dimX),
	  dimY(dimY),
	  area(dimX * dimY),
	  nodeType(nodeType),
	  roiArea(0.0),
	  roiRadius(0.0),
	  roiRadiusSquared(0.0),
	  roiDimX(0.0),
	  roiDimY(0.0),
	  halfDiagonalLength(0.0),
	  weight(weight),
	  state(NODE_STATE_START)
	{
		if(nodeType == NODE_TYPE_TERMINAL)
			state = NODE_STATE_STABLE;

		halfDiagonalLength = sqrt(dimX * dimX + dimY * dimY);
	}

	inline void addNet(int netId)
	{
		nets.push_back(netId);
	}

	inline int getNetId(int index)
	{
		return nets[index];
	}

	inline bool isTerminal(void) const
	{
		return nodeType == NODE_TYPE_TERMINAL;
	}

	inline bool isNonTerminal(void) const
	{
		return nodeType == NODE_TYPE_NON_TERMINAL;
	}

	inline bool isSudo (void) const
	{
		return nodeType == NODE_TYPE_SUDO;
	}

	inline void move(void)
	{
		int newState = NODE_STATE_STABLE;

		if((velocity + permanentVelocity).getLengthSquared() > MIN_VELOCITY)
		{
			pos.x += velocity.x + permanentVelocity.x;
			pos.y += velocity.y + permanentVelocity.y;

			newState = newState | NODE_STATE_MOVING;
		}

		if(force.getLengthSquared() > MIN_FORCE)
		{
			newState = newState | NODE_STATE_ACCELERATING;
		}

		state = newState;
		printShort();

		force.reset();
	}

	inline Position calcTopLeftPos(void)
	{
		return Position(pos.x - dimX / 2, pos.y - dimY / 2);
	}

	inline Position calcTopRightPos(void)
	{
		return Position(pos.x + dimX / 2, pos.y - dimY / 2);
	}

	inline Position calcBottomLeftPos(void)
	{
		return Position(pos.x - dimX / 2, pos.y + dimY / 2);
	}

	inline Position calcBottomRightPos(void)
	{
		return Position(pos.x + dimX / 2, pos.y + dimY / 2);
	}

	inline Position calcROITopLeftPos(void)
	{
		return Position(pos.x - roiDimX / 2, pos.y - roiDimY / 2);
	}

	inline Position calcROITopRightPos(void)
	{
		return Position(pos.x + roiDimX / 2, pos.y - roiDimY / 2);
	}

	inline Position calcROIBottomLeftPos(void)
	{
		return Position(pos.x - roiDimX / 2, pos.y + roiDimY / 2);
	}

	inline Position calcROIBottomRightPos(void)
	{
		return Position(pos.x + roiDimX / 2, pos.y + roiDimY / 2);
	}

	/**
	 * F = m x a
	 * V = a x t [t -> defined in MainConfiguration.h]
	 */
	inline void calcVelocity(void)
	{
		velocity += (force / weight) * t;
		//force.reset(); TODO
	}

	/**
	 * Apply additional force that is originated from air/water density.
	 * It's direction is the opposite of node's velocity and calculated by the following formula;
	 *
	 * R = 1/2 x (D x p x A x v^2) [D, p -> defined in MainConfiguration.h]
	 *
	 * where;
	 * R: resistance
	 * D: resistance multiplier. For circular objects it is 0.5 and can go up to 2.
	 * p: air/water density
	 * A: surface area of the node that is perpendicular to the movement
	 * v: velocity of the object
	 *
	 */
	inline void calcResistance(void)
	{
		// resistance-magnitude = 0.5 * D * p * area * velocity.getLengthSquared();
		// resistance-direction = velocity.getNormalized.getNegated();
		// resistance = resistance-direction * resistance-magnitude;

		resistance = velocity.getNormalized().getNegated() *
					0.5 * D * p * area * velocity.getLengthSquared();
	}

	inline void calcROI(const double& ratio)
	{
		roiArea = ratio * area * (1 - DENSITY) + area * DENSITY;

		// For circle based ROI
		roiRadiusSquared = roiArea / PI;
		roiRadius = sqrt(roiRadiusSquared);

		// For rectangle based ROI
		double edgeMultiplier = sqrt(roiArea / area);
		roiDimX = dimX * edgeMultiplier;
		roiDimY = dimY * edgeMultiplier;
	}

	std::string toString(void)
	{
		std::stringstream converter;
		converter << *this;

		return converter.str();
	}

	inline void addPermanentVelocity(Force& force)
	{
		permanentVelocity += (force / weight) * t;
	}

	inline bool isStable(void)
	{
		return state == NODE_STATE_STABLE;
	}

	inline void printShort(void)
	{
		std::cout << "n" << id << "=> ";
		std::cout << "pos" << pos;

		if(nodeType != NODE_TYPE_TERMINAL)
		{
			Force v = velocity + permanentVelocity;

			int fDir = force.getDirBinary(MIN_FORCE);
			int vDir = v.getDirBinary(MIN_VELOCITY);


			int vDirX = vDir & BINARY_FORCE_X_MASK;
			int vDirY = vDir & BINARY_FORCE_Y_MASK;
			int fDirX = fDir & BINARY_FORCE_X_MASK;
			int fDirY = fDir & BINARY_FORCE_Y_MASK;

			if(vDirX == fDirX && vDirY == fDirY)
			{
				std::cout << " accelerating both X and Y";
			}
			else if(vDirX == fDirX)
			{
				std::cout << " accelerating only X";
			}
			else if(vDirY == fDirY)
			{
				std::cout << " accelerating only Y";
			}
			else if(vDirX != fDirX && vDirY != fDirY)
			{
				std::cout << " de-accelerating";
			}

			std::cout << " state: " << state;

		}
		else
		{
			std::cout << " Terminal";
		}

		std::cout << std::endl;
	}

	unsigned int id;
	double dimX;
	double dimY;
	double area;
	int nodeType;
	double roiArea;
	double roiRadiusSquared;
	double roiRadius;
	double roiDimX;
	double roiDimY;
	double halfDiagonalLength;
	int weight;
	int state;

	Position pos;
	Force velocity;
	Force permanentVelocity;
	Force resistance;
	Force force;
	std::vector<int> nets;
};

#endif /* NODE_H_ */
