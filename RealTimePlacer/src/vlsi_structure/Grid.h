/*
 * Grid.h
 *
 *  Created on: Jun 11, 2013
 *      Author: matara
 */

#ifndef GRID_H_
#define GRID_H_

#include <ostream>

#include "Graph.h"
#include "GridCell.h"
#include "../boid/Force.h"

#define grid_iterate_all_cells_ij_ptr \
	for(int i = 0; i < grid->rows; ++i) \
		for(int j = 0; j < grid->cols; ++j)

#define grid_iterate_all_cells_ij_ref \
	for(int i = 0; i < grid.rows; ++i) \
		for(int j = 0; j < grid.cols; ++j)

#define grid_iterate_all_cells_ij_self \
	for(int i = 0; i < rows; ++i) \
		for(int j = 0; j < cols; ++j)


struct Grid
{
	friend std::ostream& operator<<(std::ostream& out, Grid& grid)
	{
		out << "Horizontal Cell Count: " << grid.cols << " Size: " << grid.cellLengthX << std::endl;
		out << "Vertical Cell Count: " << grid.rows << " Size: " << grid.cellLengthY << std::endl;
		out << std::endl;
		grid_iterate_all_cells_ij_ref
			out << *grid(i, j) << std::endl;
		return out;
	}

	// Constructors
	// -------------------------------------------------------------------------------------------------------

	Grid(Graph* graph, int rows, int cols,
			double cellLengthX, double cellLengthY,
			double boardLengthX, double boardLengthY)
	: graph(graph),
	  cols(cols),
	  rows(rows),
	  cellLengthX(cellLengthX),
	  cellLengthY(cellLengthY),
	  boardLengthX(boardLengthX),
	  boardLengthY(boardLengthY),
	  cellArea(cellLengthX * cellLengthY),
	  gridCells(0)
	{
		gridCells = new GridCell*[cols * rows];

		for(int i = 0; i < rows; ++i)
			for(int j = 0; j < cols; ++j)
			{
				Position topLeft(i * cellLengthX, j * cellLengthY);
				Position topRight((i + 1) * cellLengthX, (j + 1) * cellLengthY);

				gridCells[i * cols + j] = new GridCell(i, j, topLeft, topRight, boardLengthX, boardLengthY);
			}

	}

	virtual ~Grid(void)
	{
		grid_iterate_all_cells_ij_self
			delete gridCells[i * cols + j];

		delete [] gridCells;
	}

	// Members
	// -----------------------------------------------------------------------------------------------

	inline GridCell* operator()(int& i, int& j)
	{
		if(!isValidCell(i, j))
			throw;

		return gridCells[i * cols + j];
	}

	inline GridCell* operator()(const int& i, const int& j)
	{
		if(!isValidCell(i, j))
			throw;

		return gridCells[i * cols + j];
	}

	void calculateDistribution(void)
	{
		// clear previous distribution data to calculate new one
		reset();

		// TODO might need a little bit optimization
		for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
		{
			Node* currNode = graph->getNode(i);
			Position nodeTopLeftCorner = currNode->calcTopLeftPos();
			Position nodeTopRightCorner = currNode->calcTopRightPos();
			Position nodeBottomLeftCorner = currNode->calcBottomLeftPos();
			Position nodeBottomRightCorner = currNode->calcBottomRightPos();

			std::cout << "CalcDistro for node: " << *currNode << std::endl;

			GridCell* topLeftCornerCell = findResidenceCell(nodeTopLeftCorner);
			GridCell* topRightCornerCell = findResidenceCell(nodeTopRightCorner);
			GridCell* bottomLeftCornerCell = findResidenceCell(nodeBottomLeftCorner);
			GridCell* bottomRightCornerCell = findResidenceCell(nodeBottomRightCorner);

			// Node is placed perfectly in cell, no cuts
			if(topLeftCornerCell == bottomRightCornerCell)
			{
				std::cout << "Node is placed perfectly in cell, no cuts" << std::endl;
				topLeftCornerCell->addNode(currNode, currNode->area);
			}
			else
			{
				// Both horizontal and vertical cut
				if(topLeftCornerCell != topRightCornerCell && topLeftCornerCell != bottomLeftCornerCell)
				{
					std::cout << "Both horizontal and vertical cut" << std::endl;
					// Calculate partition on top-left grid-cell
					double areaInTopLeftCell = _calculateArea(nodeTopLeftCorner, topRightCornerCell->bottomRightPoisiton);
					topLeftCornerCell->addNode(currNode, areaInTopLeftCell);

					// Calculate partition on top-right grid-cell
					double areaInTopRightCell = _calculateArea(nodeTopRightCorner, topRightCornerCell->bottomLeftPosition);
					topRightCornerCell->addNode(currNode, areaInTopRightCell);

					// Calculate partition on bottom-left grid-cell
					double areaInBottomLeftCell = _calculateArea(nodeBottomLeftCorner, bottomLeftCornerCell->topRightPosition);
					bottomLeftCornerCell->addNode(currNode, areaInBottomLeftCell);

					// Calculate partition on bottom-right grid-cell
					double areaInBottomRightCell = _calculateArea(nodeBottomRightCorner, bottomRightCornerCell->topLeftPosition);
					bottomRightCornerCell->addNode(currNode, areaInBottomRightCell);

				}
				else if(topLeftCornerCell != topRightCornerCell) // Vertical cut
				{
					std::cout << "Vertical cut" << std::endl;
					// Calculate partition on the left
					Position bottomRightOnTheLeft(topLeftCornerCell->bottomRightPoisiton.x, nodeBottomLeftCorner.y);
					double areaOnLeft = _calculateArea(nodeTopLeftCorner, bottomRightOnTheLeft);
					topLeftCornerCell->addNode(currNode, areaOnLeft);

					// Calculate partition on the right
					Position topLeftOnTheRight(topRightCornerCell->topLeftPosition.x, nodeTopLeftCorner.y);
					double areaOnRight = _calculateArea(topLeftOnTheRight, nodeBottomRightCorner);
					topRightCornerCell->addNode(currNode, areaOnRight);

				}
				else if(topLeftCornerCell != bottomLeftCornerCell) // Horizontal cut
				{
					std::cout << "Horizontal cut" << std::endl;
					// Calculate partition on the top half
					Position bottomRightOnTheTop(nodeTopRightCorner.x, topLeftCornerCell->bottomRightPoisiton.y);
					double areaOnTop = _calculateArea(nodeTopLeftCorner, bottomRightOnTheTop);
					topLeftCornerCell->addNode(currNode, areaOnTop);

					// Calculate partition on the bottom half
					Position topLeftOnTheBottom(nodeTopLeftCorner.x, bottomRightCornerCell->topLeftPosition.y);
					double areaOnBottom = _calculateArea(topLeftOnTheBottom, nodeBottomRightCorner);
					bottomRightCornerCell->addNode(currNode, areaOnBottom);
				}
			}
		}
	}

	/**
	 * DEPRECATED
	 * Calculates the density of each cell. If a center point of node is in some cell,
	 * than all the surface area of the node is added only to that particular cell. So,
	 * no boundary situations are considered in this method
	 */
	void calculateDistributionDumn(void)
	{
		// clear previous distribution data to calculate new one
		reset();

		std::cout << "Calculating Distribution !!" << std::endl;

		// traverse each node in graph and update the grid-cell it belongs to
		for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
		{
			Node* currNode = graph->getNode(i);
			GridCell* residence = findResidenceCell(currNode->pos);
			residence->addNode(currNode, currNode->area);
		}

		// after calculating cell distribution, calculate density per cell
		// and determine each cells state
		grid_iterate_all_cells_ij_self
		{
			GridCell* cellAtij = (*this)(i, j);
			cellAtij->calculateDensity(cellArea);
		}
	}

	/**
	 * Updates the state of each cell. Currently state determines if a cell
	 * will generate current or not.
	 *
	 * STABLE: will not generate any current
	 * ACTIVE: will generate currents
	 */
	/*
	inline void updateCellStates(double& lowerBound, double& upperBound)
	{
		grid_iterate_all_cells_ij_self
		{
			GridCell* cellAtij = (*this)(i, j);
			if(lowerBound <= cellAtij->density && cellAtij->density <= upperBound)
				cellAtij->state = GRIDCELL_STATE_STABLE;
			else
				cellAtij->state = GRIDCELL_STATE_ACTIVE;
		}
	}
	*/
	/**
	 * Calculates the 4 forces for each cell on the 4 different corner
	 */
	void calculateCurrents(void)
	{
		grid_iterate_all_cells_ij_self
		{
			GridCell* cellAtij = (*this)(i, j);
			calculateCurrent(cellAtij);
		}
	}

	/**
	 * For a cell calculate the forces it applies to nodes contained within it.
	 * There are 4 forces in total, one in each corner.
	 */
	void calculateCurrent(GridCell* gridCell)
	{
		// If cell is stable, then it cannot generate any currents but it is still affected by
		// other cells, and node movement. So its state is changeable at any moment.
		if(gridCell->isStable())
		{
			gridCell->topLeftForce.reset();
			gridCell->topRightForce.reset();
			gridCell->bottomLeftForce.reset();
			gridCell->bottomRightForce.reset();
		}
		else // cell is still active, calculate the currents
		{
			// Find the neighbor cells first
			int left[2]; left[0] = gridCell->rowId - 1; left[1] = gridCell->colId;
			int right[2]; right[0] = gridCell->rowId + 1; right[1] = gridCell->colId;
			int top[2]; top[0] = gridCell->rowId; top[1] = gridCell->colId - 1;
			int bottom[2]; bottom[0] = gridCell->rowId; bottom[1] = gridCell->colId + 1;

			// TODO maybe a constant would be nice to multiply with the density difference between cells.

			Force leftForce = calculateForce(gridCell, left[0], left[1], FORCE_DIRECTION_LEFT);
			Force rightForce = calculateForce(gridCell, right[0], right[1], FORCE_DIRECTION_RIGHT);
			Force topForce = calculateForce(gridCell, top[0], top[1], FORCE_DIRECTION_UP);
			Force bottomForce = calculateForce(gridCell, bottom[0], bottom[1], FORCE_DIRECTION_DOWN);


			// std::cout << "Calculated forces; " << std::endl;
			// std::cout << "Left" << leftForce << std::endl;
			// std::cout << "Right" << rightForce << std::endl;
			// std::cout << "Top" << topForce << std::endl;
			// std::cout << "Bottom" << bottomForce << std::endl;

			// calculate the force at each corner and set it

			gridCell->topLeftForce = leftForce + &topForce;
			gridCell->topRightForce = rightForce + &topForce;
			gridCell->bottomLeftForce = leftForce + &bottomForce;
			gridCell->bottomRightForce = rightForce + &bottomForce;

			// std::cout << "Top Left" << gridCell->topLeftForce << std::endl;
			// std::cout << "Top Right" << gridCell->topRightForce << std::endl;
			// std::cout << "Bottom Left" << gridCell->bottomLeftForce << std::endl;
			// std::cout << "Bottom Right" << gridCell->bottomRightForce << std::endl;
		}
	}

	/**
	 * Calculates the force between two cells. If neighbor is not a valid cell, then magnitude is
	 * set to zero.
	 */
	inline Force calculateForce(GridCell* currCell, int neighborRow, int neighborCol, int direction)
	{
		Force force;
		if(direction == FORCE_DIRECTION_LEFT)
			force.x = -1;
		else if(direction == FORCE_DIRECTION_RIGHT)
			force.x = 1;
		else if(direction == FORCE_DIRECTION_UP)
			force.y = 1;
		else if(direction == FORCE_DIRECTION_DOWN)
			force.y = -1;

		if(isValidCell(neighborRow, neighborCol))
		{
			GridCell* neighbor = (*this)(neighborRow, neighborCol);
			double magnitude = currCell->density - neighbor->density;

			force *= magnitude;
		}

		return force;
	}

	/**
	 * Checks if a given cell id falls into boundaries or not.
	 */
	inline bool isValidCell(const int& cellIdRow, const int& cellIdCol)
	{
		if(cellIdRow >= rows || cellIdRow < 0 ||
			cellIdCol >= cols || cellIdCol < 0)
			return false;
		return true;
	}

	/**
	 * Finds the cell that the given point resides in.
	 */
	GridCell* findResidenceCell(Position& point)
	{
		std::cout << "findResidenceCell: " << point;

		int cellIdX = 0;
		double posX = point.x - cellLengthX;
		while(posX > 0)
		{
			++cellIdX;
			posX -= cellLengthX;
		}

		int cellIdY = 0;
		double posY = point.y - cellLengthY;
		while(posY > 0)
		{
			++cellIdY;
			posY -= cellLengthY;
		}

		std::cout << " (" << cellIdY << ", " << cellIdX << ")" << std::endl;

		return (*this)(cellIdY, cellIdX);
	}

	/**
	 * Resets all the satellite data in grid-cells.
	 */
	void reset(void)
	{
		grid_iterate_all_cells_ij_self
		{
			// std::cout << "reset i: " << i << " j: " << j << std::endl;
			(*this)(i, j)->reset();
		}
	}

	/**
	 * Prints shorter version of itself
	 */
	void printDistributionOnly(void)
	{
		std::cout << "Board size (" << cols * cellLengthX << ", " << rows * cellLengthY << ")" << std::endl;
		grid_iterate_all_cells_ij_self
		{
			GridCell* currCell = (*this)(i, j);
			std::cout << "[" << currCell->rowId << ", " << currCell->colId << "]:";

			for(unsigned int i = 0; i < currCell->nodes.size(); ++i)
				std::cout << " " << currCell->nodes[i];

			std::cout << " (Density: " << currCell->density << ")" << std::endl;
		}
	}

	/**
	 * calculates the area of rectangle specified by its top-left and
	 * bottom-right coordinates.
	 */
	inline double _calculateArea(Position& topLeft, Position& bottomRight)
	{
		return (bottomRight.x - topLeft.x) * (bottomRight.y - topLeft.y);
	}

	/**
	 * If any point of any node is out of board, push it back in.
	 */
	void _legalize(void)
	{
		for(unsigned int i = 0; i < graph->getTotalNodeCount(); ++i)
			_legalizeNode(graph->getNode(i));
	}

	/**
	 * If a partition of specified node lays out of board, then adjust its position.
	 */
	inline void _legalizeNode(Node *node)
	{
		Position nodeTopLeftPos = node->calcTopLeftPos();
		Position nodeBottomRightPos = node->calcBottomRightPos();

		if(nodeTopLeftPos.x < 0)
			node->pos.x += -nodeTopLeftPos.x; // shift right
		else if(nodeBottomRightPos.x > boardLengthX)
			node->pos.x -= (nodeBottomRightPos.x - boardLengthX); // shift left

		if(nodeTopLeftPos.y < 0)
			node->pos.y += -nodeTopLeftPos.y; // shift down
		else if(nodeBottomRightPos.y > boardLengthY)
			node->pos.y -= (nodeBottomRightPos.y - boardLengthY); // shift up

	}

	Graph* graph;
	int cols;
	int rows;
	double cellLengthX;
	double cellLengthY;
	double boardLengthX;
	double boardLengthY;
	double cellArea;
	GridCell** gridCells;
};

#endif /* GRID_H_ */
