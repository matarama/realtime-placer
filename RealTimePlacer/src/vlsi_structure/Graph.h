/*
 * Graph.h
 *
 *  Created on: Jun 6, 2013
 *      Author: matara
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include <vector>
#include <ostream>

#include "../data_structure/SparseMatrix.h"
#include "Node.h"
#include "Net.h"

class Graph : public SparseMatrix
{
	friend std::ostream& operator<<(std::ostream& out, Graph& graph)
	{
		out << "Net Count: " << graph.totalNetCount << std::endl;
		out << "Node Count: " << graph.totalNodeCount << std::endl;
		out << "Sudo-Node Count: " << graph.sudoNodes.size() << std::endl;

		SparseMatrix& base(graph);
		out << base << std::endl;

		out << "Detailed node info: " << std::endl;
		for(unsigned int i = 0; i < graph.nodes.size(); ++i)
			out << *graph.nodes[i] << std::endl;

		out << std::endl;

		out << "Detailed net info: " << std::endl;
		for(unsigned int i = 0; i < graph.nets.size(); ++i)
			out << *graph.nets[i] << std::endl;

		return out;
	}

	friend std::ostream& operator<<(std::ostream &out, Graph* graph)
	{
		return operator<<(out, *graph);
	}

public:
	Graph(void)
	: totalNetCount(0),
	  totalNodeCount(0),
	  terminalCount(0),
	  sudoNodeCount(0),
	  totalCellArea(0.0)
	{
	}

	virtual ~Graph(void)
	{
		for(unsigned int i = 0; i < nodes.size(); ++i)
			delete nodes[i];

		for(unsigned int i = 0; i < nets.size(); ++i)
			delete nets[i];
	}

	inline void addNode(unsigned int nodeId, double dimX, double dimY, int weight, int nodeType = NODE_TYPE_NON_TERMINAL)
	{
		SparseMatrix::addItem(nodeId);

		Node* node = new Node(nodeId, dimX, dimY, weight, nodeType);
		nodes.push_back(node);

		++totalNodeCount;
		 if(node->nodeType == NODE_TYPE_TERMINAL)
			++terminalCount;

		totalCellArea += dimX * dimY;
	}

	inline void addRelation(int v1, int v2, int netId, double weight = 1.0)
	{
		(*this)(v1, v2) += weight;
		(*this)(v2, v1) += weight;
	}

	inline Net* addNet(int netId)
	{
		++totalNetCount;

		Net *net = new Net((unsigned int) netId);
		nets.push_back(net);

		return net;
	}

	inline unsigned int addSudoNode(int netId, int weight)
	{
		unsigned int sudoNodeId = getTotalNodeCount();
		addNode(sudoNodeId, 0.0, 0.0, weight, NODE_TYPE_SUDO);
		sudoNodes.push_back(sudoNodeId);
		++sudoNodeCount;

		nodes[sudoNodeId]->addNet(netId);

		return sudoNodeId;
	}

	inline void addNodeToNet(int nodeId, int netId)
	{
		nodes[(unsigned int) nodeId]->addNet(netId);
	}

	inline void addNetToNode(int netId, int nodeId)
	{
		nets[(unsigned int) netId]->addNode(nodeId);
	}

	inline Net* getNet(unsigned int netId)
	{
		return nets[netId];
	}

	inline Node* getNode(unsigned int nodeId)
	{
		return nodes[nodeId];
	}

	void extractNetRelationTable(void)
	{
	}

	SparseMatrix* getNetRelationTable(void)
	{
		return 0;
	}

	unsigned int getTerminalCount(void)
	{
		return terminalCount;
	}

	unsigned int getNonTerminalCount(void)
	{
		return totalNodeCount - terminalCount;
	}

	unsigned int getTotalNodeCount(void)
	{
		return totalNodeCount;
	}

	unsigned int getTotalNetCount(void)
	{
		return totalNetCount;
	}

	double getTotalCellArea(void)
	{
		return totalCellArea;
	}

	double getSudoNodeCount(void)
	{
		return sudoNodeCount;
	}

	void printShortInfo(void)
	{
		for(unsigned int i = 0; i < totalNodeCount; ++i)
		{
			Node* n = getNode(i);
			n->printShort();
		}
	}

private:
	unsigned int totalNetCount;
	unsigned int totalNodeCount;
	unsigned int terminalCount;
	unsigned int sudoNodeCount;
	double totalCellArea;
	std::vector<unsigned> sudoNodes;
	std::vector<Node*> nodes;
	std::vector<Net*> nets;
};

#endif /* GRAPH_H_ */
